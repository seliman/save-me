
function logout()
{
    localStorage.clear();
    window.location = '../dashboard/login.html';
}

document.writeln('<script src="../js/jquery-2.1.4.min.js"></script>');
document.writeln('<script src="../js/bootstrap.min.js"></script>');
document.writeln('<script src="../lib/ionic/js/ionic.bundle.js"></script>');
document.writeln('<script src="../js/really-simple-jquery-dialog.js"></script>');
document.writeln('<script src="../cordova.js"></script>');
document.writeln('<script src="../includes/defines.js"></script>');
document.writeln('<script src="../js/config.js"></script>');
document.writeln('<script src="../js/server.js"></script>');
//document.writeln('<script src="../js/sth-select.js"></script>');
document.writeln('<script src="../js/waitMe.js"></script>');
document.writeln('<script src="../js/ptrLight.js"></script>');
document.writeln('<script src="../js/jquery.rtResponsiveTables.js"></script>');
document.writeln('<script src="../js/app.js"></script>');
document.writeln('<script src="../js/loginCtrl.js"></script>');
document.writeln('<script src="../js/signupCtrl.js"></script>');
document.writeln('<script src="../js/sidemenu.js"></script>');
document.writeln('<script src="../js/menuCtrl.js"></script>');
document.writeln('<script src="../js/homeCtrl.js"></script>');
document.writeln('<script src="../js/carlistCtrl.js"></script>');
document.writeln('<script src="../js/addcarCtrl.js"></script>');
document.writeln('<script src="../js/carinfoCtrl.js"></script>');
document.writeln('<script src="../js/statisticsCtrl.js"></script>');
document.writeln('<script src="../js/stakeholdersCtrl.js"></script>');
document.writeln('<script src="../js/accidentsCtrl.js"></script>');
document.writeln('<script src="../js/profileCtrl.js"></script>');
document.writeln('<script src="../js/notificationCtrl.js"></script>');
document.writeln('<script src="../js/notification_listCtrl.js"></script>');


//Ambulance Controllers 
document.writeln('<script src="../js/ambulance_sidemenu.js"></script>');
document.writeln('<script src="../js/ambulance_menuCtrl.js"></script>');
document.writeln('<script src="../js/ambulance_homeCtrl.js"></script>');
document.writeln('<script src="../js/ambulance_profileCtrl.js"></script>');
document.writeln('<script src="../js/ambulance_editCtrl.js"></script>');
document.writeln('<script src="../js/ambulance_listCtrl.js"></script>');
document.writeln('<script src="../js/ambulance_notificationCtrl.js"></script>');
document.writeln('<script src="../js/ambulance_notification_listCtrl.js"></script>');

//Police Controllers 
document.writeln('<script src="../js/police_sidemenu.js"></script>');
document.writeln('<script src="../js/police_menuCtrl.js"></script>');
document.writeln('<script src="../js/police_homeCtrl.js"></script>');
document.writeln('<script src="../js/police_profileCtrl.js"></script>');
document.writeln('<script src="../js/police_notificationCtrl.js"></script>');
document.writeln('<script src="../js/police_notification_listCtrl.js"></script>');

//Insurance Controllers 
document.writeln('<script src="../js/insurance_sidemenu.js"></script>');
document.writeln('<script src="../js/insurance_menuCtrl.js"></script>');
document.writeln('<script src="../js/insurance_homeCtrl.js"></script>');
document.writeln('<script src="../js/maps.js"></script>');
document.writeln('<script src="../js/Chart.min.js"></script>');
document.writeln('<script src="../js/utils.js"></script>');
document.writeln('<script src="../js/insurance_profileCtrl.js"></script>');
document.writeln('<script src="../js/insurance_notificationCtrl.js"></script>');
document.writeln('<script src="../js/insurance_notification_listCtrl.js"></script>');
document.writeln('<script src="../js/policyCtrl.js"></script>');
document.writeln('<script src="../js/vehicle_coverageCtrl.js"></script>');
document.writeln('<script src="../js/driver_coverageCtrl.js"></script>');

document.writeln('<script src="../js/jquery.flot.js"></script>');
document.writeln('<script src="../js/jquery.flot.resize.js"></script>');
document.writeln('<script src="../js/jquery.flot.pie.js"></script>');
document.writeln('<script src="../js/jquery.flot.categories.js"></script>');