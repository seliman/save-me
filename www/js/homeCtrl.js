angular.module('sidemenu.controllers')
.controller('homeCtrl', function($scope) 
{
    
    $('li > a > div').removeClass('submenu_active');

   // alert(distance('33.5570691','35.372948','33.80455','35.588779','M'));
   //startTrack();
   

    $("#get_accident_info").on('click',function()
    {  
          enableBluetooth();
          
          cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled)
          {
            if(enabled == true)
            {
                getAccidentInfo();
            }
            else
            {
                alert('Please Enable GPS');
            }
          }, function(error){
            alert("The following error occurred: "+error);
          });
        
        //getAccidentInfo();
    });
    
    $("#manual_accident").on('click',function()
    {
        enableBluetooth();
          
          cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled)
          {
            if(enabled == true)
            {
                setStorage('other_stakeholders',data);
                getAccidentInfo(2);
            }
            else
            {
                alert('Please Enable GPS');
            }
          }, function(error){
            alert("The following error occurred: "+error);
          });
    });
   


    $("#driver_mode").on('click',function()
    {
      
        if($("#driver_mode").attr('src') == '../img/driving_on.svg')
        {
            $("#driver_mode").attr('src','../img/driving_off.svg');
            $("#driver_mode").attr('data-toggle','collapse');
            $("#driver_mode").attr('aria-expanded','false');
            $("#driver_mode").addClass('dropdown-toggle');
            
            event.stopPropagation();
            event.preventDefault();
    
            var drop = $(this).closest(".dropdown");
            $(drop).addClass("open");
    
            $('.collapse.in').collapse('hide');
            var col_id = $(this).attr("href");
            $(col_id).collapse('toggle');
    
        }
        else
        {
            $("#driver_mode").attr('src','../img/driving_on.svg');
            
            //alert('stop driving');
            $("#car_name").text('');
            $("#car_name_bold").text('');
            setStorage('start_driving','');
            setStorage('driving_mode','');
            stopTrack();
        }

        
    });


    var modal = document.getElementById('accidentModal');

    // Get the button that opens the modal
    var btn = document.getElementById("accident_img");
    
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    
    // When the user clicks the button, open the modal 
    btn.onclick = function() {
        modal.style.display = "block";
    }
    
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
    
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    
    if(getStorage('start_driving')!='' && getStorage('start_driving')!=null && getStorage('start_driving')!='null')
    {
        $("#car_list_prompt").attr('src','../img/driving_off.svg');
        $("#car_name").text(getStorage('driving_mode'));
        $("#car_name_bold").text('Driving Mode');
    }
    else
    {
        $("#car_list_prompt").attr('src','../img/driving_on.svg');
        $("#car_name").text('');
        $("#car_name_bold").text('');
    }

    $("#car_list_prompt").on('click',function()
    {
        
        /*bluetoothSerial.isConnected(
            function() {
                alert("Bluetooth is connected");
            },
            function() {
                alert("Bluetooth is *not* connected");
            }
        );*/

        if($("#car_list_prompt").attr('src') == '../img/driving_off.svg')
        {
            
            $("#car_list_prompt").attr('src','../img/driving_on.svg');
            $("#car_name").text('');
            $("#car_name_bold").text('');
            setStorage('start_driving','');
            setStorage('driving_mode','');
            disconnectHardware();
        }
        else
        {
            var car_counter = 0;
            $.each(car_info,function(index){
                if(car_info[index].length>0)
                car_counter++;
            });
            if(car_counter>0)
            {
                carlistprompt('Choose Car' , 'body prompt' ,'text','current_username',function(data)
                {
                    if(data[0]=='' || data[0]=='null' || data[0]==null)
                    {
                        alert('Please Select a car');
                    }
                    else
                    {
                        cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled)
                        {
                            if(enabled == true)
                            {
                                
                                startDriving(data[0],data[1]);
                            }
                            else
                            {
                                alert('Please Enable GPS');
                            }
                        }, function(error){
                            alert("The following error occurred: "+error);
                        });
                    }
                    
                });
            }
            else
            {
                alert('No Car(s) Found');
            }
        }
        
    });
    

    $("#myaccident").on('click',function()
    {   
        cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled)
        {
          if(enabled == true)
          {
             
              if(getStorage('start_driving')=='' || getStorage('start_driving')=='null' || getStorage('start_driving')==null)
              {
                alert('Please Select a Car as Driving Mode');
              }
              else
              {
                    getAccidentInfo(1);
              }
              
          }
          else
          {
              alert('Please Enable GPS');
          }
        }, function(error){
          alert("The following error occurred: "+error);
        });
       
        modal.style.display = "none";
        
    });


    $("#manual").on('click',function()
    {
        var counter = 0;
        checkboxprompt('Choose Stakeholders' , 'body prompt' ,'text','current_username', function(data)
        {
            if(data == '')
            {
                alert('No Stakeholder(s) choosed');
            }
            else
            {
                cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled)
                {
                  if(enabled == true)
                  { 
                        setStorage('other_stakeholders',data);
                        getAccidentInfo(2);
                  }
                  else
                  {
                      alert('Please Enable GPS');
                  }
                }, function(error){
                  alert("The following error occurred: "+error);
                });
            }
            /*confirm('Send Accident','Are you sure?',function()
            {  
                if(counter<1)
                { 
                    getAccidentInfo(2);
                }
            });*/
            
        });
        modal.style.display = "none";
    });

    /*$("#accident_img").on('click',function()
    {

        if($("#accident_img").attr('src') == '../img/collision_off.svg')
        {
            $("#accident_img").attr('src','../img/collision_on.svg');
        }
        else
        {
            $("#accident_img").attr('src','../img/collision_off.svg');
        }
        
    });*/


   /**************************************************************************************************************** */

    $("#bluetooth_enabled").on('click',function(){
        bluetoothEnabled(function(enabled){            
        },function(error){alert(error);});

     });

     $("#enable_bluetooth").on('click',function(){
        enableBluetooth();
     });
     
     $("#gps_enabled").on('click',function(){
        GPSEnabled();
     });

     $("#send_mail").on('click',function()
     {
         sendMail('','','',function(data){
            alert(data.success);
         });  
     });

    $("#get_weather").on('click',function()
    {
        startLoading('');
        currentPosition(function(position){
           /* alert('Latitude: '          + position.coords.latitude          + '\n' +
            'Longitude: '         + position.coords.longitude         + '\n' +
            'Altitude: '          + position.coords.altitude          + '\n' +
            'Accuracy: '          + position.coords.accuracy          + '\n' +
            'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
            'Heading: '           + position.coords.heading           + '\n' +
            'Speed: '             + position.coords.speed             + '\n' +
            'Timestamp: '         + new Date(position.timestamp)      + '\n');*/
            
       getWeatherInfo(position.coords.latitude,position.coords.longitude,function(data){
        alert(
            "id: "+data['weather'][0].id + '\n'+
            "main: "+data['weather'][0].main + '\n'+
            "description: "+data['weather'][0].description + '\n'
        );
    }); 
        },function(error)
        {
            alert(error);
        }); 
    });
     
    /**************************    HARDWARE MONITORING     *******************************/

    $("#hardware_monitoring").on('click',function()
    {
        hardwareMoitoring();
    });

    var app = {
        sendSms: function() {
           /* var number = document.getElementById('numberTxt').value.toString();*/ /* iOS: ensure number is actually a string */
            /* var message = document.getElementById('messageTxt').value; */
          
            //CONFIGURATION
    
            var number = "phone number";
            var message = "message";
    
    
            var options = {
                replaceLineBreaks: false, // true to replace \n by a new line, false by default
                
            };
    
            var success = function () { alert('Message sent successfully'); };
            var error = function (e) { alert('Message Failed:' + e); };
            sms.send(number, message, options, success, error);
        }
    };

    
   
})
