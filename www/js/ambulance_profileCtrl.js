angular.module('ambulance_sidemenu.controllers')
.controller('ambulance_profileCtrl', function($scope) 
{
    var user_id = getStorage('user_id');
    var username = getStorage("username");
    var profile_password = '';
    var email = getStorage("email");
    var new_password = '';

    $("#profile_username").text(username);
    $("#profile_email").text(email);

    $("#change_username").on('click',function()
    {
        var current_username = $("#profile_username").text();

        prompt('Edit Username' , 'body prompt' ,'text',current_username, function(data)
        {
            var param = 'user_id='+user_id+"&username="+data;
            request(check_username_url,param,function(result){
                if(result['success'] == -1)
                {
                    alert('Username Already exists');
                }
                else if(result['success'] == 1)
                {
                    
                    change('username' , data);
                }
            })
            
           
        });
        //$("#profile_username").text($("#b").val());

    });

    $("#change_password").on('click',function()
    {
        var current_password = $("#profile_password").text();

        prompt('Old Password' , 'body prompt' ,'password','', function(data)
        {
            
            var link = check_pass_link;
            var params = "user_id="+user_id+"&old_password="+data
            request(link , params , function(values)
            {
                    if(values['success']==1)
                    {
                        change('password' , data);
                    }
                    else
                    {
                        alert('Invalid Password');
                    }
            });
             //change('password' , data);
           
        });
        //$("#profile_username").text($("#b").val());

    });



    $("#change_email").on('click',function()
    {
        
        var current_email = $("#profile_email").text();

        prompt('Edit Email' , 'body prompt' ,'text',current_email, function(data)
        {
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!filter.test(data)) 
            {
                alert('Invalid Email Entry');
            }
            else
            {
               change('email' , data);
            }
        });
        

    });

    $("#change_mobile").on('click',function()
    {
        var current_mobile = $("#profile_mobile").text();

        prompt('Edit Phone Number' , 'body prompt' ,'number',current_mobile, function(data)
        {
            change('mobile' , data);
            
        });
        

    });

    function change(key , value)
    {
        var old_password = '';
        if(key == 'password')
        {
            // check if old pass is correct 
            prompt('Change Password' , 'body prompt' ,'password','', function(data)
            {
                var lowerCaseLetters = /[a-z]/g;            
                var upperCaseLetters = /[A-Z]/g;
                var numbers = /[0-9]/g;
                var validate_lowercase = (data.match(lowerCaseLetters))?true:false;
                var validate_uppercase = data.match(upperCaseLetters)?true:false;
                var validate_numbers = data.match(numbers)?true:false;

                if(validate_lowercase==false || validate_uppercase==false || validate_numbers==false || data.length<8)
                {
                    alert('Password must be at least 8 characters with Lower,Upper characters and Numbers');
                   
                }
                else{
                    old_password = data;
                    prompt('Confirm Password' , 'body prompt' ,'password','', function(data)
                    {
                        if(old_password != data)
                        {
                            alert('Password not Matched');
                        }
                        else
                        {
                            new_password = data;
                        }  
                    });   
                } 
            });
        }
        else
        $("#profile_"+key).text(value);
    }
    
$("#ambulance_save_changes").on('click' , function()
{
    
  
    var profile_username = $("#profile_username").text();
    var profile_email = $("#profile_email").text();
    var password_changed = (new_password=='')?0:1;

    var params ="username="+profile_username+"&password="+new_password+"&email="+profile_email
    +"&userid="+user_id+"&password_changed="+password_changed;

    var link = edit_stakeholders_profile_url;
     
        request(link,params,function(data)
        {
            if(data['success']==1)
            {
                setStorage("username",profile_username);
               // setStorage("password",profile_password);
                setStorage("email",profile_email);
                $("#menu_username").text(getStorage("username"));

                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace(" active", "");
                $("#menu_home").addClass('active');
                $("#homeSubmenu").removeClass('in');
                $("#navigate").attr('href','#/app/home');

                alertCallback('Profile Edit Successfully','',function()
                {
                         $("#navigate").click();
                });
            }
            else
            alert('An Error Ocurred');
        },'');
    
    
    
});



})