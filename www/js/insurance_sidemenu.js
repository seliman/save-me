angular.module('insurance_sidemenu', ['insurance_sidemenu.controllers' , 'ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    
  
   
    $ionicPlatform.registerBackButtonAction(function () 
    {
        //history.go(-(history.length - 1));
        //window.history.go(-4); 
        ionic.Platform.exitApp();

    }, 100);

  });
  
   $ionicPlatform.registerBackButtonAction(function (event) {
      if($state.current.name=="app.home"){
        navigator.app.exitApp(); //<-- remove this line to disable the exit
      }
      else {
        navigator.app.backHistory();
      }
    }, 100);

   
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: '../insurance/menu.html',
    controller: 'insurance_menuCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: '../insurance/home.html',
        controller: 'insurance_homeCtrl'
      }
    }
  })

  .state('app.profile', {
    url: '/profile',
    views: {
      'menuContent': {
        templateUrl: '../insurance/profile.html',
        controller: 'insurance_profileCtrl'
      }
    }
  })

  .state('app.notification', {
    url: '/notification',
    views: {
      'menuContent': {
        templateUrl: '../insurance/insurance_notification.html',
        controller: 'insurance_notificationCtrl'
      }
    }
  })

  .state('app.notification_list', {
    url: '/notification_list',
    views: {
      'menuContent': {
        templateUrl: '../insurance/insurance_notification_list.html',
        controller: 'insurance_notification_listCtrl'
      }
    }
  })

  .state('app.policy', {
    url: '/policy',
    views: {
      'menuContent': {
        templateUrl: '../insurance/policy.html',
        controller: 'policyCtrl'
      }
    }
  })

  .state('app.vehicle_coverage', {
    url: '/vehicle_coverage',
    views: {
      'menuContent': {
        templateUrl: '../insurance/vehicle_coverage.html',
        controller: 'vehicle_coverageCtrl'
      }
    }
  })

  .state('app.driver_coverage', {
    url: '/driver_coverage',
    views: {
      'menuContent': {
        templateUrl: '../insurance/driver_coverage.html',
        controller: 'driver_coverageCtrl'
      }
    }
  })
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/policy');
});