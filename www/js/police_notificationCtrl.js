angular.module('police_sidemenu.controllers')
.controller('police_notificationCtrl', function($scope,$ionicSideMenuDelegate) 
{  
    var params = 'stakeholder_id=2&user_id='+getStorage('user_id');
    
    request(get_notifications_url,params,function(data)
    {
        if(data['success'] == -1)
        {
            $("#notifications_list").css('display','none');
            $("#no_notifications").css('display','block');
        }
        else if(data['success'] == 1)
        {
            $.each(data['notifications'],function(index)
            {
                var notification_href="?subject="+encodeURIComponent(data['notifications'][index].subject)
                +"&body="+encodeURIComponent(data['notifications'][index].body)+"&day="
                +encodeURIComponent(data['notifications'][index].name)+"&date="
                +encodeURIComponent(data['notifications'][index].notification_date)+"&time="
                +encodeURIComponent(data['notifications'][index].notification_time)+"&lat="
                +encodeURIComponent(data['notifications'][index].latitude)+"&lng="
                +encodeURIComponent(data['notifications'][index].longitude)+"&city="
                +encodeURIComponent(data['notifications'][index].city)+"&country="
                +encodeURIComponent(data['notifications'][index].country)+"&user_id="
                +encodeURIComponent(data['notifications'][index].user_id);

                var content = '<a class="item item-avatar" href="#/app/notification_list'+notification_href+'">';
                content += '<img src="../img/notification.svg">';
                content += '<span style="float:right;">'+data['notifications'][index].name+' '+data['notifications'][index].notification_date+' </span>';
                content += '<h2>'+'<b>'+data['notifications'][index].subject+'</b>'+'</h2>';
                content += '<span style="float:right;">'+data['notifications'][index].notification_time+'</span>';
                content += '<p>'+data['notifications'][index].body+'</p>';
                content += '</a>';

                $("#notifications_list").append(content);
            });
        }
        else
        {
            alert('An error occurred');
        }
        
    });
    

});