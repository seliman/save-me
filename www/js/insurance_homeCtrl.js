angular.module('insurance_sidemenu.controllers')
.controller('insurance_homeCtrl', function($scope) 
{
    

    $("#get_latlng").on('click',function()
    {
     /*   $.ajax(
            {
            url: '../js/test.js',
            dataType: 'script',
            async: false,
        beforeSend: function()
        {
            startLoading('');
        },
        complete: function(){
            stopLoading();
        },
        success:function()
        {
            
        }
        }).done(function(){alert(json);});*/
       
        
        var onSuccess = function(position) {
            alert('Latitude: '          + position.coords.latitude          + '\n' +
                  'Longitude: '         + position.coords.longitude         + '\n' +
                  'Altitude: '          + position.coords.altitude          + '\n' +
                  'Accuracy: '          + position.coords.accuracy          + '\n' +
                  'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
                  'Heading: '           + position.coords.heading           + '\n' +
                  'Speed: '             + position.coords.speed             + '\n' +
                  'Timestamp: '         + position.timestamp                + '\n');
        };
     
        // onError Callback receives a PositionError object
        //
        function onError(error) {
            alert('code: '    + error.code    + '\n' +
                  'message: ' + error.message + '\n');
        }
     
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
        
        
    })

































    var x = [];
    var y = [];
    var z = [];
    var g = [];

    $("#testapi").on('click',function()
    {
        
        currentPosition(function(position)
        {
           
            /*$.getJSON('https://nominatim.openstreetmap.org/reverse?json_callback=?&format=json', {lat: position.coords.latitude, lon: position.coords.longitude}, function(data) {
                alert(data.address.country+" "+data.address.town+" "+data.address.state);
                stopLoading();
            });*/
            var request = new XMLHttpRequest();

            var method = 'GET';
            var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+position.coords.latitude+','+position.coords.longitude+'&sensor=true&key=AIzaSyAXX46Kf8xIz7ZPaZf8VABw3WPYUq0l8tg';
            var async = true;
    
            request.open(method, url, async);
            request.onreadystatechange = function(){
              if(request.readyState == 4 && request.status == 200){
                var data = JSON.parse(request.responseText);
                var address = data.results[0];
                alert(address['address_components'][0].long_name + " "+address['address_components'][1].long_name+" "+address['address_components'][2].long_name );
              }
            };
            request.send();

          

         },function(error)
         {
             alert(error);
         }); 

    });

    $("#accelerometer").on('click',function()
    {
        //accelerometer_list

        var counter = 0;
     var acceleration_interval = window.setInterval(function()
        {
            getAccelerometer(function(acceleration)
            {
                var acceleration_x = acceleration.x;
                var acceleration_y = acceleration.y;
                var acceleration_z = acceleration.z;
                var timestamp = acceleration.timestamp;
                var gravity = Math.sqrt((acceleration_x*acceleration_x)+(acceleration_y*acceleration_y)+(acceleration_z*acceleration_z));
                /*var content = '<p>';
                content += 'X: '+acceleration_x+" Y: "+acceleration_y+" Z:"+acceleration_z+" Timestamp: "+timestamp+" G: "+gravity+" c:"+counter;
                content += '</p>';
                $("#accelerometer_list").append(content);*/
                $("#counter").html(counter+" s");
                x.push(acceleration_x);
                y.push(acceleration_y);
                z.push(acceleration_z);
                g.push(gravity);
                counter++;
                if(counter>14)
                {
                    // draw the graph
                    clearInterval(acceleration_interval);
                    var DATA_COUNT = 16;
    var labels = [];

    Samples.srand(4);

    for (var i = 0; i < DATA_COUNT; ++i) 
    {
        labels.push('' + i);
    }

    Chart.helpers.merge(Chart.defaults.global, 
    {
        aspectRatio: 4/3,
        tooltips: false,
        layout: {
            padding: {
                top: 32,
                right: 24,
                bottom: 32,
                left: 0
            }
        },
        elements: {
            line: {
                fill: false
            }
        },
        plugins: {
            legend: true,
            title: false
        }
    });


    var chart = new Chart('chart-0', {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label:"X",
                backgroundColor: Samples.color(0),
                borderColor: Samples.color(0),
                data:x
                
            }, {
                label:"Y",
                backgroundColor: Samples.color(1),
                borderColor: Samples.color(1),
                data: y
            }, {
                label:"Z",
                backgroundColor: Samples.color(2),
                borderColor: Samples.color(2),
                data:z,
                datalabels: {
                    align: 'end',
                    anchor: 'end'
                }
            }, {
                label:"Gravity",
                backgroundColor: "#e78200",
                borderColor: "#cc0000",
                data:g,
                datalabels: {
                    align: 'end',
                    anchor: 'end'
                }
            }]
        },
        options: {
            plugins: {
                datalabels: {
                    align: function(context) {
                        var index = context.dataIndex;
                        var curr = context.dataset.data[index];
                        var prev = context.dataset.data[index - 1];
                        var next = context.dataset.data[index + 1];
                        return prev < curr && next < curr ? 'end' :
                            prev > curr && next > curr ? 'start' :
                            'center';
                    },
                    backgroundColor: 'rgba(255, 255, 255, 0.7)',
                    borderColor: 'rgba(128, 128, 128, 0.7)',
                    borderRadius: 4,
                    borderWidth: 1,
                    color: function(context) {
                        var i = context.dataIndex;
                        var value = context.dataset.data[i];
                        var prev = context.dataset.data[i - 1];
                        var diff = prev !== undefined ? value - prev : 0;
                        return diff < 0 ? Samples.color(0) :
                            diff > 0 ? Samples.color(1) :
                            'gray';
                    },
                    font: {
                        size: 11,
                        weight: 600
                    },
                    offset: 8,
                    formatter: function(value, context) {
                        var i = context.dataIndex;
                        var prev = context.dataset.data[i - 1];
                        var diff = prev !== undefined ? prev - value : 0;
                        var glyph = diff < 0 ? '\u25B2' : diff > 0 ? '\u25BC' : '\u25C6';
                        return glyph + ' ' + Math.round(value);
                    }
                }
            }
        }
    });
                }
            });
          }, 1000);

          //clearInterval(acceleration_interval);
    });

    
}) 
