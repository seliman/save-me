angular.module('police_sidemenu.controllers')
.controller('police_notification_listCtrl', function($scope,$ionicSideMenuDelegate) 
{  
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });

    var subject =decodeURIComponent(vars['subject']);
    var body = decodeURIComponent(vars['body']);
    var day = decodeURIComponent(vars['day']);
    var time = decodeURIComponent(vars['time']);
    var date = decodeURIComponent(vars['date']);
    var lat = decodeURIComponent(vars['lat']);
    var lng = decodeURIComponent(vars['lng']);
    var city = decodeURIComponent(vars['city']);
    var country = decodeURIComponent(vars['country']);
    var user_id = decodeURIComponent(vars['user_id']);
    
    $("#date").text(day+" "+date);
    $("#time").text(time);
    $("#subject").append(subject);
    $("#notification_body").append(body);

    if(user_id!='' && user_id!=null && user_id!='null')
    {
        $("#no_map").css('display','none');
        $("#city").text('Admin Notification');
        $("#country").text('');
    }
    else
    {
        $("#city").text(city);
        $("#country").text(country);
    }
    
    $("#go_map").on('click',function()
    {
            $("#go_map").attr('href',"http://maps.google.com/maps?q="+lat+","+lng);
            $("#go_map").click();
    }); 

    $.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyAXX46Kf8xIz7ZPaZf8VABw3WPYUq0l8tg',function(){
        var map = new google.maps.Map(document.getElementById('map'), 
        {
          zoom: 8,
          center: {lat: 33.6149908, lng: 35.8464298},
          travelMode: google.maps.TravelMode.DRIVING
        });
        
        var myLatlng = new google.maps.LatLng(lat,lng);
        var marker = new google.maps.Marker(
        {
        position: myLatlng
        });
    
        marker.setMap(map);
    
        google.maps.event.addListener(marker, 'click', function() 
        {
            /*$("#go_map").attr('href',"http://maps.google.com/maps?q="+lat+","+lng);
            $("#go_map").click();*/
            window.location.href="http://maps.google.com/maps?q="+lat+","+lng;
            //alert('s');
        });
    })
});