angular.module('ambulance_sidemenu.controllers')
.controller('ambulance_editCtrl', function($scope) 
{

    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
 
    var id = vars['id'];
    var action = vars['action'];
    var action_txt = '';
   // alert(id + " "+action);
   
    if(action == 1)
    {
        $("#car_action").text('Edit Accident');
        $("#edit_victim").text('Save Accident');
        action_txt = 'Accident Edit Successfully';
        var victim_name = decodeURIComponent(vars['name']);
        var category = decodeURIComponent(vars['category']);
        var injury = decodeURIComponent(vars['injury']);
        var hospital = decodeURIComponent(vars['hospital']);
        var info = decodeURIComponent(vars['info']);
        var status = decodeURIComponent(vars['status']);
        var status_name = decodeURIComponent(vars['status_name']);
        var complaint = decodeURIComponent(vars['complaint']);
        var victim_age = decodeURIComponent(vars['age']);
        var victim_cause = decodeURIComponent(vars['cause']);
        
        if(victim_name != 'null' && victim_name!='')
        {
            $("#victim_name").val(victim_name);
        }

        if(victim_age != 'null' && victim_age!='')
        {
            $("#victim_age").val(victim_age);
        }

        if(victim_cause != 'null' && victim_cause!='')
        {
            $("#victim_cause").val(victim_cause);
        }

        if(category != 'null' && category!='')
        {
            addDropDown('victim_category','Select Category','Select Category',ambulance_victim_category_dropdown,decodeURIComponent(vars['category_name']));
            $("#victim_category").val(category);
        }    
        else
        {
            addDropDown('victim_category','Select Category','Select Category',ambulance_victim_category_dropdown);
        }

        if(injury != 'null' && injury!='')
        {
            addDropDown('injury_severity','Select Severity','Select Severity',ambulance_injury_severity_dropdown,decodeURIComponent(vars['injury_name']));
            $("#injury_severity").val(injury);
        }
        else
        {
            addDropDown('injury_severity','Select Severity','Select Severity',ambulance_injury_severity_dropdown);
        }

        if(hospital != 'null' && hospital!='')
        {
            addDropDown('hospital','Select Hospital','Select Hospital',ambulance_hospital_dropdown,decodeURIComponent(vars['hospital_name']));
            $("#hospital").val(hospital);
        }
        else
        {
            addDropDown('hospital','Select Hospital','Select Hospital',ambulance_hospital_dropdown);
        }
        
        if(status != 'null' && status!='')
        {
            addDropDown('patient_status','Select Patient Status','Select Patient Status',patient_status_dropdown,decodeURIComponent(vars['status_name']));
            $("#patient_status").val(status);
        }
        else
        {
            addDropDown('patient_status','Select Patient Status','Select Patient Status',patient_status_dropdown);
        }

        if(complaint != 'null' && complaint!='')
        {
            $("#chief_complaint").val(complaint);
        }

        if(info != 'null' && info!='')
        {
            $("#add_info").text(info);
        }
        
    }
    else if(action ==2)
    {
        $("#car_action").text('Add Accident');    
        $("#edit_victim").text('Add Accident');
        action_txt = 'Accident Added Successfully';
            
        addDropDown('victim_category','Select Category','Select Category',ambulance_victim_category_dropdown);
        addDropDown('injury_severity','Select Severity','Select Severity',ambulance_injury_severity_dropdown);
        addDropDown('hospital','Select Hospital','Select Hospital',ambulance_hospital_dropdown);
        addDropDown('patient_status','Select Patient Status','Select Patient Status',patient_status_dropdown);
    }

    $.getScript("../js/sth-select.js", function(){});

    $("#edit_victim").on('click',function()
    {
        var vcategory = $("#victim_category").val();
        var vinjury = $("#injury_severity").val();
        var vhospital = $("#hospital").val();
        var additional_info = $("#add_info").val();
        var victim_name = $("#victim_name").val();
        var patient_status = $("#patient_status").val();
        var complaint = $("#chief_complaint").val();
        var age = $("#victim_age").val();
        var cause = $("#victim_cause").val();

        if(vcategory == -1 || vinjury == -1 || vhospital ==-1 ||patient_status==-1|| victim_name=='' || victim_name=='null'||complaint=='' || complaint=='null'||age=='' || age=='null'||cause=='' || cause=='null')
        {
            alert('Please fill required fields');
        }
        else
        {
                var params = 'id='+id+"&victim_category="+vcategory
                +"&injury_severity="+vinjury+"&hospital="+vhospital+"&info="+additional_info+"&action="+action
                +"&victim_name="+victim_name+"&status="+patient_status+"&complaint="+complaint+"&age="+age+"&cause="+cause;
           
            //edit_accident_url
            request(edit_accident_url,params,function(data)
            {
                
                if(data['success']==1)
                {
                    $("#navigate").attr('href','#/app/home');

                    alertCallback(action_txt,'',function()
                    {
                             $("#navigate").click();
                    });
                }
                else
                {
                    alert('An Error Occurred');
                }

            });
            
        }

    });

});