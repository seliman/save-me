function Back()
{
    window.history.go(-1);
}
function reload_page()
{
   window.location.href='#/app/menu_policy';
}

setStorage("start_driving",'');
function alert(title = '' , body = '')
{
    $('#testAlert').simpleAlert({
        title:title,
        message: body
    });
}

function alertCallback(title,body,success,failure)
{
    
    $('#testConfirm').simpleAlertCallback({
        title: title,
        message: body,
        success:success,
        cancel: failure
    });
}

function confirm(title = '' , body = '' , success , failure)
{
    $('#testConfirm').simpleConfirm({
        title: title,
        message: body,
        success:success,
        cancel: failure
    });
}

function prompt(title='' , message = '' ,type, value, success)
{
    $('#testPrompt').simplePrompt({
        title:title,
        message: "",
        type:type,
        value:value,
        success: success
    });

}

function checkboxprompt(title='' , message = '' ,type, value, success)
{
    $('#checkPrompt').checkboxPrompt({
        title:title,
        message: "",
        type:type,
        value:value,
        success: success
    });

}

function carlistprompt(title='' , message = '' ,type, value, success)
{
    $('#carlistPrompt').carlistprompt({
        title:title,
        message: "",
        type:type,
        value:value,
        success: success
    });

}

function startLoading(text,reference="body")
{
    $("#"+reference).waitMe({
            effect: 'roundBounce',
            text: text,
            bg: 'rgba(255,255,255,0.7)',
            color: '#083b66',
            fontWeight:'bold',
            onClose: function() {}
    });
}

function stopLoading(reference="body")
{
    $("#"+reference).waitMe('hide');
}

function refresh()
{
    $(function() 
    {
        $('#content').ptrLight({
            'refresh': function(ptrLightInstance) 
            {
                //ptrLightInstance.done();
                location.reload();
            },
            pullThreshold: $(window).height() * 0.5,
            maxPullThreshold: $(window).height()
        });
    });
}

function getParams() 
{
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function setStorage(key , value)
{
    localStorage.setItem(key , value);
}

function getStorage(key)
{
   return localStorage.getItem(key);
}

function clearStorage()
{
    localStorage.clear();
}

function addDropDown(id,title,placeholder,data,placeholder_title=-1)
{
    var content = '<div id="div_"'+id+' class="row item item-input item-stacked-label" style="border-top: none;border-bottom: none;">';
    content += '<label for="simplest"></label>'; 
    if(placeholder_title==-1)
        content +='<select id="'+id+'" name="simplest" sth-select sth-select-title="'+title+'" sth-select-placeholder="'+title+'" sth-select-autosize="false">';
    else
        content +='<select id="'+id+'" name="simplest" sth-select sth-select-title="'+title+'" sth-select-placeholder="'+placeholder_title+'" sth-select-autosize="false">';
    content += '<option value="-1" selected>'+placeholder+'</option>';
    $.each(data, function(index)
    { 
        content += '<option value="'+data[index].id+'">'+data[index].value+'</option>';
    });
    
    content +='<select>';
    content +='</div>';
    
    $("#content_"+id).append(content);
    
}

function navigate(path_id , href_path)
{
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    $("#"+path_id).addClass('active');
    $("#homeSubmenu").removeClass('in');
    $("#navigate").attr('href',href_path);
    $("#navigate").click();
}

////////////////////////////////////////////////////////////////
function ssssss(day,month,year , user_type= getStorage('user_type'))
{
    
    var counter = 0;
    $('#accident_tbody').empty();
    $("#table_details").css('display','block');

    $.each(accidents_array[day+"-"+month+"-"+year], function(index) 
    {
        counter++;
        var table_content = '<tr>'; 
        table_content += '<td>'+accidents_array[day+"-"+month+"-"+year][index][6]+'</td>';
        table_content += '<td>'+accidents_array[day+"-"+month+"-"+year][index][6]+'</td>';
        table_content += '<td>'+accidents_array[day+"-"+month+"-"+year][index][6]+'</td>';
        table_content += '<td>'+accidents_array[day+"-"+month+"-"+year][index][6]+'</td>';
        table_content += '<td>'+accidents_array[day+"-"+month+"-"+year][index][6]+'</td>';
        table_content += '<td>'+accidents_array[day+"-"+month+"-"+year][index][6]+'</td>';
        table_content += '<td>'+accidents_array[day+"-"+month+"-"+year][index][6]+'</td>';
        table_content += '<td>'+accidents_array[day+"-"+month+"-"+year][index][6]+'</td>';
        table_content += '<td>'+accidents_array[day+"-"+month+"-"+year][index][6]+'</td>';
        if(user_type == 2)
        table_content += '<td>'+"<button class='button button-icon icon ion-edit' style='color:#e78200;' onclick='edit("+accidents_array[day+"-"+month+"-"+year][index][0]+")'></button>"+'</td>';               
        table_content +='</tr>';
        $("#accident_tbody").append(table_content);
    },'');

    $("#total_accidents").text(counter);

    $("#search").on('keyup',function()
    {
        var total = 0;
        $("#accident_tbody").empty();
        var search_text = $("#search").val();
        var search_length = $("#search").val().length;
        
        if(search_text == '')
        {
            $.each(data['accidents_calender'], function(index)
            {
                    var table_content = '<tr>'; 
                    table_content += '<td>'+data['accidents_calender'][index].Accident_date+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].day+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].Accident_time+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].city+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].country+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].street+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].light+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].weather+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].Speed_limit+'</td>';
                    if(user_type == 2)
                    table_content += '<td>'+"<button class='button button-icon icon ion-edit' style='color:#e78200;' onclick='edit("+data['accidents_calender'][index].idVictim+")'></button>"+'</td>';               
                    table_content +='</tr>';
                    $("#accident_tbody").append(table_content);
                    total++;

            });
            
            $("#total_accidents").text(total);
        }

        else
        {
            
            $.each(data['accidents_calender'], function(index)
            {
            
                if( ((data['accidents_calender'][index].city.slice(0 , search_length)).toLowerCase() == search_text.toLowerCase()) 
                || ((data['accidents_calender'][index].Accident_date.slice(0 , search_length)) == search_text)
                || ((data['accidents_calender'][index].day.slice(0 , search_length)).toLowerCase() == search_text.toLowerCase())
                || ((data['accidents_calender'][index].Accident_time.slice(0 , search_length)) == search_text))
                {
                    var table_content = '<tr>'; 
                    table_content += '<td>'+data['accidents_calender'][index].Accident_date+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].day+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].Accident_time+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].city+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].country+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].street+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].light+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].weather+'</td>';
                    table_content += '<td>'+data['accidents_calender'][index].Speed_limit+'</td>';
                    if(user_type == 2)
                    table_content += '<td>'+"<button class='button button-icon icon ion-edit' style='color:#e78200;' onclick='edit("+data['accidents_calender'][index].idVictim+")'></button>"+'</td>';              
                    table_content +='</tr>';
                    $("#accident_tbody").append(table_content);
                    total++;
                }

            });

            $("#total_accidents").text(total);
        }
    }); 
}

function getAccidentCalenderInfo(day,month,year , user_type= getStorage('user_type'))
{
    var params = "month="+month+"&year="+year+"&day="+day;
        
    request(accidents_calender_url,params,function(data)
    {
        if(data['success'] == -1)
        {
            $("#table_details").css('display','none');
            
        }
        else if(data['success'] == 1)
        {
            
            var counter = 0;
            $('#accident_tbody').empty();
            $("#table_details").css('display','block');

            $.each(data['accidents_calender'], function(index) 
            {
                counter++;
                var table_content = '<tr>'; 
                table_content += '<td>'+data['accidents_calender'][index].Accident_date+'</td>';
                table_content += '<td>'+data['accidents_calender'][index].day+'</td>';
                table_content += '<td>'+data['accidents_calender'][index].Accident_time+'</td>';
                table_content += '<td>'+data['accidents_calender'][index].city+'</td>';
                table_content += '<td>'+data['accidents_calender'][index].country+'</td>';
                table_content += '<td>'+data['accidents_calender'][index].street+'</td>';
                table_content += '<td>'+data['accidents_calender'][index].light+'</td>';
                table_content += '<td>'+data['accidents_calender'][index].weather+'</td>';
                table_content += '<td>'+data['accidents_calender'][index].Speed_limit+'</td>';
                if(user_type == 2)
                table_content += '<td>'+"<a class='button button-icon icon ion-android-list' style='color:#083b66;' href='#/app/list?id="+data['accidents_calender'][index].idAccident_info+"' ></a>"+'</td>';               
                table_content +='</tr>';
                $("#accident_tbody").append(table_content);
            },'');

            $("#total_accidents").text(counter);
        
            $("#search").on('keyup',function()
            {
                var total = 0;
                $("#accident_tbody").empty();
                var search_text = $("#search").val();
                var search_length = $("#search").val().length;
                
                if(search_text == '')
                {
                    $.each(data['accidents_calender'], function(index)
                    {
                            var table_content = '<tr>'; 
                            table_content += '<td>'+data['accidents_calender'][index].Accident_date+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].day+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].Accident_time+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].city+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].country+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].street+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].light+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].weather+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].Speed_limit+'</td>';
                            if(user_type == 2)
                            table_content += '<td>'+"<button class='button button-icon icon ion-edit' style='color:#e78200;' onclick='edit("+data['accidents_calender'][index].idVictim+")'></button>"+'</td>';               
                            table_content +='</tr>';
                            $("#accident_tbody").append(table_content);
                            total++;
        
                    });
                    
                    $("#total_accidents").text(total);
                }
        
                else
                {
                    
                    $.each(data['accidents_calender'], function(index)
                    {
                    
                        if( ((data['accidents_calender'][index].city.slice(0 , search_length)).toLowerCase() == search_text.toLowerCase()) 
                        || ((data['accidents_calender'][index].Accident_date.slice(0 , search_length)) == search_text)
                        || ((data['accidents_calender'][index].day.slice(0 , search_length)).toLowerCase() == search_text.toLowerCase())
                        || ((data['accidents_calender'][index].Accident_time.slice(0 , search_length)) == search_text))
                        {
                            var table_content = '<tr>'; 
                            table_content += '<td>'+data['accidents_calender'][index].Accident_date+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].day+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].Accident_time+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].city+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].country+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].street+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].light+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].weather+'</td>';
                            table_content += '<td>'+data['accidents_calender'][index].Speed_limit+'</td>';
                            if(user_type == 2)
                            table_content += '<td>'+"<button class='button button-icon icon ion-edit' style='color:#e78200;' onclick='edit("+data['accidents_calender'][index].idVictim+")'></button>"+'</td>';              
                            table_content +='</tr>';
                            $("#accident_tbody").append(table_content);
                            total++;
                        }
        
                    });
        
                    $("#total_accidents").text(total);
                }
            }); 
            //filterTable();
        }
        else if(data['success'] == 0)
        {
            alert('An Error Occurred');
        }
    });

}


//////////////////////////////////////////////////////////////////////////////////////////
    
function openCamera(success , fail)
{
    document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {
        navigator.camera.getPicture(success, fail, { quality: 25,
            destinationType: Camera.DestinationType.DATA_URL
        });
    
    }
} 

function openGallery(success , fail)
{
    navigator.camera.getPicture(success, fail, {
    	  destinationType: navigator.camera.DestinationType.FILE_URI,
    	  sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY
    });   
}

function getCurrentLocation()
{
    var latlng = [];
    if ("geolocation" in navigator){ //check geolocation available 
        //try to get user current location using getCurrentPosition() method
        navigator.geolocation.getCurrentPosition(function(position)
        { 
             latlng.push('s');
            
            return latlng;
               // alert("Lat : "+position.coords.latitude+" </br>Lng :"+ position.coords.longitude);
            });
    }else{
        alert("Browser doesn't support geolocation!");
    }

    
}

function currentPosition(success , fail)
{
    /*var options = {
        maximumAge: 3600000,
        timeout: 3000,
        enableHighAccuracy: true,
     }
     var watchID = navigator.geolocation.watchPosition(success, fail, options);*/
    
    if (navigator.geolocation) navigator.geolocation.getCurrentPosition(success,fail);
}

function checkConnection() 
{
    var networkState = navigator.connection.type;
 
    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';
 
    alert('','Connection type: ' + states[networkState]);
}

function TurnFlashLightOn()
{
    window.plugins.flashlight.available(function(isAvailable) {
        if (isAvailable) {
      
          // switch on
          window.plugins.flashlight.switchOn(
            function() {}, // optional success callback
            function() {}, // optional error callback
            {intensity: 0.3} // optional as well
          );
      
      
        } else {
          alert("Flashlight not available on this device");
        }
      });
}

function TurnFlashLightOff()
{
    window.plugins.flashlight.switchOff(); // success/error callbacks may be passed
}
 

function enableGps()
{
    //cordova plugin add cordova-plugin-request-location-accuracy
    cordova.plugins.locationAccuracy.canRequest(function(canRequest)
    {
        if(canRequest){
            cordova.plugins.locationAccuracy.request(function(){
                alert("Request successful");
            }, function (error){
                alert("Request failed");
                if(error){
                    // Android only
                    alert("error code="+error.code+"; error message="+error.message);
                    if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
                        if(window.confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")){
                            cordova.plugins.diagnostic.switchToLocationSettings();
                        }
                    }
                }
            }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY // iOS will ignore this
            );
        }
    });
}

function enableBackground()
{
    //cordova plugin add cordova-plugin-background-mode

    cordova.plugins.backgroundMode.enable();
            
            cordova.plugins.backgroundMode.on('activate', function () 
            {
                setInterval(function () {
                    cordova.plugins.notification.badge.increase();
                }, 1000);
            });

            cordova.plugins.backgroundMode.on('deactivate', function () 
            {
                cordova.plugins.notification.badge.clear();
             
            });
}

function callPnNumber(number,success,fail,bypassAppChooser)
{
    window.plugins.CallNumber.callNumber(success, fail, number, bypassAppChooser = true);
}

function getAccelerometer(success,fail)
{
    //cordova plugin add cordova-plugin-device-motion
    navigator.accelerometer.getCurrentAcceleration(success, fail);
}

function vibrate(time)
{
    //cordova plugin add cordova-plugin-vibration
    navigator.vibrate(time);
}

function speech(text_speech)
{
    TTS.speak({ 
        text: text_speech,
        locale: 'en-us',
        rate: 0.65}, function ()
       {
           // alert('success');
       }, function (reason) 
       {
           alert(reason);
       });
}

function deviceInfo()
{
    //cordova plugin add cordova-plugin-device
    var platform = device.platform;
    var version = device.version;

    alert(platform+"  "+version);
}

function getWeatherInfo(lat , long , callback)
{
    var api = "http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+long+"&appid=86e5e22ccd389368bd882b3bdd0390b9";
    $.ajax({
        type:"POST",
        url:api,
        dataType: "json",
        cache:false,
        beforeSend: function()
        {
            //startLoading('');
        },
        complete: function(){
            stopLoading();
        },
        success:function(data)
        {
            callback(data);
        },
        error:function(data)
        {
            alert("Unable to connect to server","","error");
        }
    });
}

function getToken()
{        
    FCMPlugin.getToken(function(token){
    
            alert(token);
    });
    
}

$(document).ready(function()
{

setTimeout(() => {
    
   //$("#test").click();
}, 1000);
//alert('ss');



//window.bluetooth.isConnected (function(){alert('connected');},function(){alert('failed');});


},false);



function toast()
{
    window.plugins.toast.showShortTop(
    'Hello!', 
    function(a)
    {
        console.log('toast success: ' + a)
    }, 
    function(b)
    {
        alert('toast error: ' + b);
    });
}



function bluetoothEnabled(success , fail)
{
    plugins.diagnostic.isBluetoothEnabled(success,fail);  
}

function enableBluetooth()
{
    cordova.plugins.BluetoothStatus.enableBT();
}

function GPSEnabled()
{
    cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled){
        alert("GPS location is " + (enabled ? "enabled" : "disabled"));
    }, function(error){
        alert("The following error occurred: "+error);
    });
}

function distance(lat1, lon1, lat2, lon2, unit) 
{
    // units --> M:miles K kilmiles N nautical miles
	var radlat1 = Math.PI * lat1/180
	var radlat2 = Math.PI * lat2/180
	var theta = lon1-lon2
	var radtheta = Math.PI * theta/180
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	if (dist > 1) {
		dist = 1;
	}
	dist = Math.acos(dist)
	dist = dist * 180/Math.PI
	dist = dist * 60 * 1.1515
	if (unit=="K") { dist = dist * 1.609344 }
	if (unit=="N") { dist = dist * 0.8684 }
	return dist
}

function startTrack()
{
  
    var options = {
        enableHighAccuracy: true,
        timeout: 30000,
        maximumAge: 0
      };
      
      tracking = navigator.geolocation.watchPosition(function(position)
      {
        
        $.each(clustering_lats,function(index)
        {
            var distances = distance(position.coords.latitude, position.coords.longitude, clustering_lats[index], clustering_lngs[index], 'M');
            
            if(distances<2)
            {
                var notify = new Audio("../sound/alert.mp3");
                notify.play();
                speech("Be carefull Dangerous Region");
            }

        })
      }, function(){alert("error");}, options);
    
}

function stopTrack()
{
    navigator.geolocation.clearWatch(tracking);
    //clearInterval(tracking);
}

///////////////////////////////////// Server /////////////////////////////////////////

function request(link,data = '' , callback, loading = '')
{

    $.ajax({
        type:"POST",
        url:link,
        data:data,
        dataType: "json",
        cache:true,
        beforeSend: function()
        {
            startLoading(loading);
        },
        complete: function(){
            stopLoading();
        },
        success:function(data)
        {
            callback(data);
        },
        error:function(request, type, errorThrown)
        {
          var message = "There was an error with the AJAX request.\n";
          switch (type) {
            case 'timeout':
              message += "The request timed out.";
              break;
            case 'notmodified':
              message += "The request was not modified but was not retrieved from the cache.";
              break;
            case 'parsererror':
              message += "XML/Json format is bad.";
              break;
            default:
              message += "HTTP Error (" + request.status + " " + request.statusText + ").";
            }
          message += "\n";
          //alert(message);
            alert("Unable to connect to server","","error");
        }
    });
}


function sendMail(link,data = '' ,loading='',callback)
{
    api = 'http://savemeapp.co/appServices/sendMail/mail.php';
    input = 'to=a&subject=b&body=c';
    $.ajax({
        type:"POST",
        url:api,
        data:input,
        dataType: "json",
        cache:true,
        beforeSend: function()
        {
            //startLoading(loading);
        },
        complete: function(){
            //stopLoading();
        },
        success:function(data)
        {
            callback(data);
        },
        error:function(data)
        {
            alert("Unable to connect to server","","error");
        }
    });
}



////////////////////////////////////////////////////////////////////////////////////

function hardwareMoitoring()
{
    var macAddress = "98:D3:51:FD:74:E0";



    bluetoothSerial.connectInsecure(
        macAddress,function() {   
          // alert('Connected Successful');  
        },
        function(err) {
           alert('There was an error Connecting to a device'+ JSON.stringify(err));  
        });
        
        bluetoothSerial.subscribe('\n', 
        function(data)
        {
           /* var result = data.split(":");
            var collision = result[1];
            var distance = result[0];

            if(collision>=25 && distance<5)
            {
                getAccidentInfo(1); 
            }*/
            if(data==1 || data=="1")
            {
                getAccidentInfo(1);   
            }
            ////////////////////////////////////
            /*if(result[0] == "d")
            { 
                   distance = result[1];
            }
            else if(result[0] == "C")
            {
                collision = result[1];
                if(result[1]>20 && distance<5)
                {
               //     getAccidentInfo(1);
                }
            }*/
            //$("#read_collision").text(result[1]);               
            //$("#read_distance").text(result[0]);
                 
            
        },function(){
                alert("fail");
        });
}

function disconnectHardware()
{
    var using_hardware = getStorage('use_hardware');

    if(using_hardware == 1)
    {
        startLoading();
        bluetoothSerial.disconnect(function(){
            stopLoading();
        },function(){ stopLoading();})
    }
}

function checkTime(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }
  
  function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    // add a zero in front of numbers<10
    m = checkTime(m);
    s = checkTime(s);
    return h + ":" + m + ":" + s;
  }

function startDriving(vehicle_id , car_name)
{
    
    cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled)
    {
        if(enabled == true)
        {
            startLoading();

             currentPosition(
            function(position)
            {
               /*position.coords.latitude,position.coords.longitude*/
                
                $("#car_name").text(car_name);
                $("#car_name_bold").text('Driving Mode');
                $("#car_list_prompt").attr('src','../img/driving_off.svg');
                setStorage('start_lat',position.coords.latitude);
                setStorage('start_lng',position.coords.longitude);
                setStorage('start_time',startTime());
                stopLoading();
                
              //  navigator.geolocation.clearWatch(watchID);

             }, function(error){alert(error.message);}); 
            
        }
        else
        {
            alert('Please Enable GPS');
        }
    }, function(error){
        alert("The following error occurred: "+error);
    });
   
    
 
    event.stopPropagation();
    event.preventDefault();

    var drop = $(this).closest(".dropdown");
    $(drop).addClass("open");

    $('.collapse.in').collapse('hide');
    var col_id = $(this).attr("href");
    $(col_id).collapse('toggle');

    

    $("#driver_mode").attr('href','');
    $("#driver_mode").attr('data-toggle','');
    $("#driver_mode").attr('aria-expanded','');
    $("#driver_mode").removeClass('dropdown-toggle');

    var checked=[];
    $.each(vehicle_stakeholder_checkbox, function(index)
    {
        if(vehicle_stakeholder_checkbox[index][0] == vehicle_id)
        {
            checked.push(vehicle_stakeholder_checkbox[index][1]);
        }
    });
    
    setStorage('start_driving',checked);
    setStorage('driving_mode',car_name); 
 
    var using_hardware = getStorage('use_hardware');

    if(using_hardware == 1)
    {
        enableBluetooth();
        hardwareMoitoring();
    }

    startTrack();
}

function getAccidentInfo(info_type=1)
{
    startLoading();
    var AccidentObject = function() 
    {
        var accident_info_array = [];
        this.addValue = function(value) 
        {
            accident_info_array.push(value);
        };
        this.example = function() {
            alert(accident_info_array[0]);
        };
        this.getAccident = function()
        {
            return accident_info_array;
        }
     }

     var accidentObj = new AccidentObject();
   
     var longlatitude = new Promise(function(resolve, reject) 
     {
         currentPosition(
              
       function(position)
        {
            /* nativegeocoder.reverseGeocode(success, failure, position.coords.latitude, position.coords.longitude, { useLocale: true, maxResults: 1 });
                function success(result) 
                {
                         resolve([position.coords.latitude,position.coords.longitude,result[0].administrativeArea,result[0].subAdministrativeArea,result[0].locality]);
                }
                function failure(err) {
                        alert(JSON.stringify(err));
                }*/


                /******************************************************************************* */
                    var request = new XMLHttpRequest();

                    var method = 'GET';
                    var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+position.coords.latitude+','+position.coords.longitude+'&sensor=true&key=AIzaSyAXX46Kf8xIz7ZPaZf8VABw3WPYUq0l8tg';
                    var async = true;
            
                    request.open(method, url, async);
                    request.onreadystatechange = function()
                    {
                    if(request.readyState == 4 && request.status == 200)
                    {
                        var data = JSON.parse(request.responseText);
                        var address = data.results[0];
                        resolve([position.coords.latitude,position.coords.longitude,address['address_components'][1].long_name,address['address_components'][2].long_name,address['address_components'][0].long_name]);
                    }
                    };
                    request.send();
                /******************************************************************************* */

          
                //navigator.geolocation.clearWatch(watchID);

         },function(error)
         {
             alert(error);
         }); 
     });
     longlatitude.then(function(value) 
     {
        accidentObj.addValue(value[1]);
        accidentObj.addValue(value[0]);
        accidentObj.addValue(value[2]);
        accidentObj.addValue(value[3]);
        accidentObj.addValue(value[4]);
        
        var weather = new Promise(function(resolve, reject) 
        {
            getWeatherInfo(accidentObj.getAccident()[0],accidentObj.getAccident()[1],function(data){
                
                resolve(data['weather'][0].description);
            });
        });
        weather.then(function(value) 
        {
            accidentObj.addValue(value); 

            /*alert(accidentObj.getAccident()[0]+" "+accidentObj.getAccident()[1]
            +" "+accidentObj.getAccident()[2]+" "+accidentObj.getAccident()[3]
            +" "+accidentObj.getAccident()[4]+" "+accidentObj.getAccident()[5]);*/
            
            var accident_longitude = (accidentObj.getAccident()[0]=='undefined')?'':accidentObj.getAccident()[0];
            var accident_latitude = (accidentObj.getAccident()[1]=='undefined')?'':accidentObj.getAccident()[1];
            var accident_city = (accidentObj.getAccident()[2]=='undefined')?'':accidentObj.getAccident()[2];
            var accident_country = (accidentObj.getAccident()[3]=='undefined')?'':accidentObj.getAccident()[3];
            var accident_street = (accidentObj.getAccident()[4]=='undefined')?'':accidentObj.getAccident()[4];
            var accident_weather = (accidentObj.getAccident()[5]=='undefined')?'':accidentObj.getAccident()[5];

            var params = 'lng='+accident_longitude+"&lat="+accident_latitude+"&city="+accident_city
            +"&country="+accident_country+"&street="+accident_street+"&weather="+accident_weather
            +"&driver_id=";
           if(info_type == 1)
           {
                params += getStorage('driver_id')+"&saved_by=";
                params += '&start_lat='+getStorage('start_lat')+'&start_lng='+getStorage('start_lng');
                params += '&start_time='+getStorage('start_time');
                if(getStorage('login_driver_dob')!='' && getStorage('login_driver_dob')!='null' && getStorage('login_driver_dob')!=null)
                params += '&driver_age='+getStorage('login_driver_dob');
           }
           else
           {
               params += '&saved_by='+getStorage('driver_id');
           }

           if(info_type==1)
                params += '&stakeholders='+getStorage('start_driving');  
           else  
                params += '&stakeholders='+getStorage('other_stakeholders');
           
                params += '&info_type='+info_type;
           
            request(add_accident_url,params,function(data)
            {
                if(data['success'] == 1)
                {
                    if(info_type==1)
                        $("#navigate").attr('href','#/app/previous_accidents');
                    else
                        $("#navigate").attr('href','#/app/statistics');
                    var current = document.getElementsByClassName("active");
                    current[0].className = current[0].className.replace(" active", "");
                    if(info_type==1)
                        $("#previous_accidents_menu").addClass('active');
                    else
                        $("#ion_item_statistics").addClass('active');
                        
                    $("#homeSubmenu").removeClass('in');

                   alertCallback('Data Added Successfully','',function()
                    {
                        setStorage('other_stakeholders','');
                    
                        $("#navigate").click();
                    });
                }
                else
                {
                    alert('An Error Occurred');
                }
            });

        });

     });


}

