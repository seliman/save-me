angular.module('police_sidemenu.controllers',['ionic'])
.controller('police_menuCtrl', function($scope,$ionicSideMenuDelegate) 
{  
    
    var btns = document.getElementsByTagName("ion-item");

    for (var i = 0; i < btns.length; i++) 
    {
        //var menu_home = document.getElementById("menu_home");
        btns[i].addEventListener("click", function() {
          var current = document.getElementsByClassName("active");
          current[0].className = current[0].className.replace(" active", "");
          this.className += " active";
          $("#homeSubmenu").removeClass('in');
        });
    }

    var params = 'user_id='+getStorage('user_id');

    request(get_stakeholders_profile_url,params,function(data)
    {
        $.each(data['stakeholders_info'], function(index) 
        {
            setStorage("username",data['stakeholders_info'][index].userName);
            setStorage("email",data['stakeholders_info'][index].Email);
        });
    });
    
});
