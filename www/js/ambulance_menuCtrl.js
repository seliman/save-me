angular.module('ambulance_sidemenu.controllers',['ionic'])
.controller('ambulance_menuCtrl', function($scope,$ionicSideMenuDelegate) 
{  
   
    var btns = document.getElementsByTagName("ion-item");

    for (var i = 0; i < btns.length; i++) 
    {
        //var menu_home = document.getElementById("menu_home");
        btns[i].addEventListener("click", function() {
          var current = document.getElementsByClassName("active");
          current[0].className = current[0].className.replace(" active", "");
          this.className += " active";
          $("#homeSubmenu").removeClass('in');
        });
    }

    var params = 'user_id='+getStorage('user_id');

    request(get_stakeholders_profile_url,params,function(data)
    {
        $.each(data['stakeholders_info'], function(index) 
        {
            setStorage("username",data['stakeholders_info'][index].userName);
            setStorage("email",data['stakeholders_info'][index].Email);
        });
       
        ambulance_hospital_dropdown = data['hospital']; 
        ambulance_injury_severity_dropdown = data['injury_severity'];
        ambulance_victim_category_dropdown = data['victim_category'];
        patient_status_dropdown = data['patient_status'];
    });

    

});
 