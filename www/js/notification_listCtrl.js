angular.module('sidemenu.controllers')
.controller('notification_listCtrl', function($scope) 
{
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });

    var subject =decodeURIComponent(vars['subject']);
    var body = decodeURIComponent(vars['body']);
    var day = decodeURIComponent(vars['day']);
    var time = decodeURIComponent(vars['time']);
    var date = decodeURIComponent(vars['date']);
    var lat = decodeURIComponent(vars['lat']);
    var lng = decodeURIComponent(vars['lng']);
    var city = decodeURIComponent(vars['city']);
    var country = decodeURIComponent(vars['country']);
    var user_id = decodeURIComponent(vars['user_id']);
    
    
    $("#date").text(day+" "+date);
    $("#time").text(time);
    $("#subject").text(subject);
    $("#notification_body").text(body);
    if(user_id!='' && user_id!=null && user_id!='null')
    {
        $("#no_map").css('display','none');
        $("#city").text('Admin Notification');
        $("#country").text('');
    }
    else
    {
        $("#city").text(city);
        $("#country").text(country);
    }

    $("#go_map").on('click',function()
    {
            $("#go_map").attr('href',"http://maps.google.com/maps?q="+lat+","+lng);
            $("#go_map").click();
          

    }); 
});