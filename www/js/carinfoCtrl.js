var vehicle_id = '';
var vehicle_model = '';
var vehicle_make = '';
var vehicle_year = '';
var vehicle_type = '';
var vehicle_nb_plate = '';
var vehicle_plate_code = '';
var vehicle_index ;
var checked_id = [];
var vehicle_type_name='';
var vehicle_year_name='';
var vehicle_code_name='';

angular.module('sidemenu.controllers')
.controller('carinfoCtrl', function($scope) 
{
  
    $.getScript("../js/sth-select.js", function(){});

    var o = getParams();
    checked_id = [];
    $.each(car_info, function(index)
    {
        if(car_info[index][0] == o['idVehicle'])
        {
            $("#vehicle_title").text(car_info[index][4]+" "+car_info[index][3]);
            $("#carinfo_model").text(car_info[index][4]);
            $("#carinfo_make").text(car_info[index][3]);
            $("#carinfo_type").text(car_info[index][2]);
            $("#carinfo_year").text(car_info[index][1]);
            $("#carinfo_plate").text(car_info[index][5]);
            $("#carinfo_code").text(car_info[index][6]);
            
            vehicle_id = car_info[index][0];
            vehicle_model = car_info[index][4];
            vehicle_make = car_info[index][3];
            vehicle_year = car_info[index][7];
            vehicle_type = car_info[index][8]
            vehicle_nb_plate = car_info[index][5];
            vehicle_plate_code = car_info[index][9];

            vehicle_type_name= car_info[index][2];
            vehicle_code_name = car_info[index][6];
            vehicle_year_name = car_info[index][1];

            vehicle_index = index;
        }

    });

   
    $('#vehicle_stakeholders li').remove();

    $.each(stakeholders_checkbox, function(index)
    {
        var content = '<li class = "item item-checkbox checkbox-energized" style="border:none;">';
        content += stakeholders_checkbox[index]['value'];
        content += '<label class = "checkbox" style="border:0;">';
        content += '<input type = "checkbox" disabled="true" value="'+stakeholders_checkbox[index]['id']+'" id="a_'+stakeholders_checkbox[index]['id']+'" style="margin-left:0px; border:0;" />';
        content += ' </label>';   
        content += '</li>';
        
        $("#vehicle_stakeholders").append(content);
                    
    });


    $.each(vehicle_stakeholder_checkbox, function(index)
    {
        if(vehicle_stakeholder_checkbox[index][0] == o['idVehicle'])
        {
           $("#a_"+vehicle_stakeholder_checkbox[index][1]).attr('checked', true);
           checked_id.push(vehicle_stakeholder_checkbox[index][1]);
        }

    });
    
    //$("input:checkbox").click(function() { return false; });
})

function getCarInfo(idVehicle)
{
    $('li > a > div').removeClass('submenu_active');
    $("#list_"+idVehicle).addClass('submenu_active');
    checked_id = [];
    
    $.each(car_info, function(index)
    {
        //alert(car_info[index][0]);
        if(car_info[index] != null)
        {
        if(car_info[index][0] == idVehicle)
        {
            $("#vehicle_title").text(car_info[index][4]+" "+car_info[index][3]);
            $("#carinfo_model").text(car_info[index][4]);
            $("#carinfo_make").text(car_info[index][3]);
            $("#carinfo_type").text(car_info[index][2]);
            $("#carinfo_year").text(car_info[index][1]);
            $("#carinfo_plate").text(car_info[index][5]);
            $("#carinfo_code").text(car_info[index][6]);
            
            vehicle_id = car_info[index][0];
            vehicle_model = car_info[index][4];
            vehicle_make = car_info[index][3];
            vehicle_index = index;

            vehicle_id = car_info[index][0];
            vehicle_model = car_info[index][4];
            vehicle_make = car_info[index][3];
            vehicle_year = car_info[index][7];
            vehicle_type = car_info[index][8]
            vehicle_nb_plate = car_info[index][5];
            vehicle_plate_code = car_info[index][9];

            vehicle_type_name= car_info[index][2];
            vehicle_code_name = car_info[index][6];
            vehicle_year_name = car_info[index][1];

            vehicle_index = index;
        }
    }

    });
    
    
    $('#vehicle_stakeholders li').remove();
    $.each(stakeholders_checkbox, function(index)
    {
        var content = '<li class = "item item-checkbox checkbox-energized" style="border:none;">';
        content += stakeholders_checkbox[index]['value'];
        content += '<label class = "checkbox" style="border:0;">';
        content += '<input type = "checkbox" disabled="true" value="'+stakeholders_checkbox[index]['id']+'" id="a_'+stakeholders_checkbox[index]['id']+'" style="margin-left:0px; border:0;" />';
        content += ' </label>';   
        content += '</li>';
        
        $("#vehicle_stakeholders").append(content);
                    
    });

    
    $.each(vehicle_stakeholder_checkbox, function(index)
    {
        if(vehicle_stakeholder_checkbox[index][0] == idVehicle)
        {
            $("#a_"+vehicle_stakeholder_checkbox[index][1]).attr('checked', true);
            checked_id.push(vehicle_stakeholder_checkbox[index][1]);
        }
    });

    $("input:checkbox").click(function() { return false; });


}

function deleteVehicle()
{


    confirm('Delete Vehicle','Are you sure you want to delete '+vehicle_model+' '+vehicle_make,function()
    {
        
           var params = 'vehicle_id='+vehicle_id;
           request(delete_vehicle_url,params,function(data)
           {
                if(data['success'] == 1)
                {
                    var current = document.getElementsByClassName("active");
                    current[0].className = current[0].className.replace(" active", "");
                    $("#menu_home").addClass('active');
                    $("#homeSubmenu").removeClass('in');
                    $("#navigate").attr('href','#/app/home');

                    car_info[vehicle_index] = new Array();
                    //car_info[vehicle_id] = null;
                    //car_info.splice(car_info,vehicle_index);
                    $("#li_"+vehicle_id).remove();
                    
                    alertCallback('Deleted Successfully','',function()
                    {
                          $("#navigate").click();
                    });
                }
                else
                {
                    alert('An Error Occurred');
                }
           })
    })
    
}



function editVehicle()
{
    
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    $("#menu_home").addClass('active');
    $("#homeSubmenu").removeClass('in');

    

    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    $("#"+"menu_carlist").addClass('active');
    $("#homeSubmenu").removeClass('in');
    $("#navigate_editcar").attr('href','#/app/add_car?id='+encodeURIComponent(vehicle_id)+"&model="+encodeURIComponent(vehicle_model)
    +"&make="+encodeURIComponent(vehicle_make)+"&type="+encodeURIComponent(vehicle_type)+"&year="+encodeURIComponent(vehicle_year)+"&platenb="+encodeURIComponent(vehicle_nb_plate)
    +"&code="+encodeURIComponent(vehicle_plate_code)+"&stakeholders="+encodeURIComponent(checked_id)
    +"&code_name="+encodeURIComponent(vehicle_code_name)+"&year_name="+encodeURIComponent(vehicle_year_name)
    +"&type_name="+encodeURIComponent(vehicle_type_name)+"&index="+encodeURIComponent(vehicle_index));
    $("#navigate_editcar").click();
    

    //navigate_editcar
}

