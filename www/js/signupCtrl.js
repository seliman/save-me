angular.module('signupCtrl', ['ionic'])
.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() 
    {
        var user_type = '';
        //getToken();

        $("#continue_signup").on('click',function()
        {
             user_type = $("#user_type").val();
            
            if(user_type == -1)
            {
                alert("Please select a type");
            }

            else
            {
                $("#content_user_type").css('display','none');
                $("#signup_content").css('display','block');
                $("#continue_signup").css('display','none');

                if(user_type != 1)
                {
                    $("#label_first_name").css('display','none');
                    $("#label_middle_name").css('display','none');
                    $("#label_last_name").css('display','none');
                    //$("#label_email").css('display','none');
                    $("#label_address").css('display','none');
                    $("#label_ssn").css('display','none');
                    $("#label_nationality").css('display','none');
                    $("#label_mobile").css('display','none');
                    $("#label_gender").css('display','none');
                    $("#label_dob").css('display','none');
                    $("#label_martial_status").css('display','none');
                    $("#label_license_date").css('display','none');
                    $("#label_license_number").css('display','none');
                    $("#label_license_state").css('display','none');
                    $("#label_username").css('border-top','none');

                }
            }
        });

        $("#signup").on('click' , function()
        {

            var first_name = $("#signup_first_name").val();
            var middle_name = $("#signup_middle_name").val();
            var last_name = $("#signup_last_name").val();
            var username = $("#signup_username").val();
            var password = $("#signup_password").val();
            var verify_password = $("#signup_verify_password").val();
            var email = $("#signup_email").val();
            var address = $("#signup_address").val();
            var ssn = $("#signup_ssn").val();
            var nationality = $("#nationality").val();
            var mobile = $("#signup_mobile").val();
            var gender = $("#gender").val();
            var dob = $("#signup_dob").val();
            var martial_status = $("#martial").val();
            var license_date = $("#signup_license_date").val();
            var license_number = $("#signup_license_number").val();
            var license_state = $("#license").val();

            /********************** Password And Email Validation **********************/

                var lowerCaseLetters = /[a-z]/g;            
                var upperCaseLetters = /[A-Z]/g;
                var numbers = /[0-9]/g;
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                
                var validate_lowercase = (password.match(lowerCaseLetters))?true:false;
                var validate_uppercase = password.match(upperCaseLetters)?true:false;
                var validate_numbers = password.match(numbers)?true:false;
               
            /****************************************************************/
            
            if(user_type==1 && (first_name == '' || middle_name =='' || last_name == '' || username == '' || password =='' 
            ||  email=='' || address == '' || ssn == '' || nationality == -1 
            || mobile == '' || gender ==-1 || dob =='' || martial_status==-1 || license_date == ''
            || license_number =='' || license_state==-1) || (user_type!=1 && username == '' || password ==''||  email==''))
            {
                alert('Please Fill All Fields');
            }

            else if(password != verify_password)
            {
                alert('Password not matched');
            }
            else if(validate_lowercase==false || validate_uppercase==false || validate_numbers==false || password.length<8)
            {
                alert('Password must be at least 8 characters with Lower,Upper characters and Numbers');
               
            }
            else if (!filter.test(email)) 
            {
                alert('Invalid email format');
            }
            else
            {
                var params = '';
                params = 'username='+username+"&password="+password+"&user_type="+user_type+"&email="+email
                   
                if(user_type == 1)
                {
                    params += '&first_name='+first_name+"&middle_name="+middle_name+"&last_name="+last_name+
                    "&address="+address+"&ssn="+ssn+"&nationality="
                    +nationality+"&mobile="+mobile+"&gender="+gender+"&dob="+dob+"&martial="+martial_status
                    +"&license_date="+license_date+"&license_number="+license_number+"&license_state="+license_state+"&login_type=1";
                }
                params+="&token=";
                var link = sign_up_link;
                 
                var promise1 = new Promise(function(resolve, reject) {
                    if(device.platform == "Android"){
                    FCMPlugin.getToken(function(token)
                    {
                    resolve(token);
                    });
                }
                else
                {
                    resolve("");
                }
                  });
                  promise1.then(function(value) {
                   params+=value;
                   request(link,params,function(data)
                   {
                       if(data['success'] == 1)
                       {
                           alertCallback('Account Created Successfully','',function()
                           {
                                 window.location = 'login.html';
                           });        
                       }
                       else if(data['success'] == -1)
                           alert('Username Already Exists');
                       else if(data['success'] == 0)
                           alert('An Error Occurred');
   
                   },'');
                  });
            } 

        });

       
    });

    $ionicPlatform.registerBackButtonAction(function () 
    {
        Back();
        
    }, 100);
        
  })
  
  
  .controller('signupCtrl', function($scope) 
  {
    
    request(signup_dropdowns_url,'',function(data)
    {
        addDropDown('user_type','Select Type','Select Type',data['user_type']);
        addDropDown('nationality','Select Nationality','Select Nationality',data['nationality']);
        addDropDown('gender','Select Gender','Select Gender',data['gender']);
        addDropDown('martial','Select Status','Select Status',data['martial']);
        addDropDown('license','Select State','Select State',data['license']);
        $.getScript("../js/sth-select.js", function(){});
    }) 

   
    

  
    
});
