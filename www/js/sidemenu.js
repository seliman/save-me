angular.module('sidemenu', ['sidemenu.controllers' , 'ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    
  
   
    $ionicPlatform.registerBackButtonAction(function () 
    {
        //history.go(-(history.length - 1));
        //window.history.go(-4); 
        ionic.Platform.exitApp();

    }, 100);

    FCMPlugin.onNotification(function(data)
   {
        /*var badge = cordova.plugins.notification.badge;
         badge.clear();*/
        
     if(data.wasTapped ==false)
     {
       // eza ken l user b2alb l app
       //alert(data.param1);
       //alert('tapped');

       var notify = new Audio("../sound/alert.mp3");
       notify.play();
     }
     else
     {
       // eza ken l user barat l app (Broadcast Receiver) in background
       //window.location.href = '#/app/notification';
     }
  
   });


  });
  
   $ionicPlatform.registerBackButtonAction(function (event) {
      if($state.current.name=="app.home"){
        navigator.app.exitApp(); //<-- remove this line to disable the exit
      }
      else {
        navigator.app.backHistory();
      }
    }, 100);

   
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: '../dashboard/menu.html',
    controller: 'menuCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: '../dashboard/home.html',
        controller: 'homeCtrl'
      }
    }
  })

  .state('app.car_list', {
    url: '/car_list',
    views: {
      'menuContent': {
        templateUrl: '../dashboard/car_list.html',
        controller: 'carlistCtrl'
      }
    }
  })

  .state('app.add_car', {
    url: '/add_car',
    views: {
      'menuContent': {
        templateUrl: '../dashboard/add_car.html',
        controller: 'addcarCtrl'
      }
    }
  })

  .state('app.carinfo', {
    url: '/carinfo',
    views: {
      'menuContent': {
        templateUrl: '../dashboard/car_info.html',
        controller: 'carinfoCtrl'
      }
    }
  })


  .state('app.statistics', {
    url: '/statistics',
    views: {
      'menuContent': {
        templateUrl: '../dashboard/statistics.html',
        controller: 'statisticsCtrl'
      }
    }
  })

  .state('app.stakeholders', {
    url: '/stakeholders',
    views: {
      'menuContent': {
        templateUrl: '../dashboard/stakeholders.html',
        controller: 'stakeholdersCtrl'
      }
    }
  })

  .state('app.previous_accidents', {
    url: '/previous_accidents',
    views: {
      'menuContent': {
        templateUrl: '../dashboard/previous_accidents.html',
        controller: 'accidentsCtrl'
      }
    }
  })

  .state('app.profile', {
    url: '/profile',
    views: {
      'menuContent': {
        templateUrl: '../dashboard/profile.html',
        controller: 'profileCtrl'
      }
    }
  })

  .state('app.notification', {
    url: '/notification',
    views: {
      'menuContent': {
        templateUrl: '../dashboard/notification.html',
        controller: 'notificationCtrl'
      }
    }
  })

  .state('app.notification_list', {
    url: '/notification_list',
    views: {
      'menuContent': {
        templateUrl: '../dashboard/notification_list.html',
        controller: 'notification_listCtrl'
      }
    }
  })

  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});