angular.module('sidemenu.controllers')
.controller('addcarCtrl', function($scope) 
{
    var vehicle_id ='';
    $('li > a > div').removeClass('submenu_active');
     
  

    $.each(stakeholders_checkbox, function(index)
    {
        var content = '<li class = "item item-checkbox checkbox-energized" style="border:none;">';
        content += stakeholders_checkbox[index]['value'];
        content += '<label class = "checkbox" style="border:0;">';
        content += '<input type = "checkbox" value="'+stakeholders_checkbox[index]['id']+'" id="'+'checkbox_'+stakeholders_checkbox[index]['id']+'" style="margin-left:0px; border:0;" />';
        content += ' </label>';   
        content += '</li>';
        
        $("#stakeholders_list").append(content);
                    
    });


    var o = getParams();
    if(o['id'] == '' || o['id'] == null || o['id'] == 'null')
    {
        addDropDown('vehicle_type','Select Type','Select Type',vehicle_type_dropdown);
        addDropDown('vehicle_year','Select Year','Select Year',vehicle_year_dropdown);
        addDropDown('plate_code','Select Type','Select Type',plate_code_dropdown);
        //$.getScript("../js/sth-select.js", function(){});

        $("#car").css('display','block');
        $("#edit_car").css('display','none');
        $("#car_action").text('Add Car');
    }
    else
    {
        $("#car_action").text('Edit Car');
        var edit_id = decodeURIComponent(o['id']);
        var edit_make = decodeURIComponent(o['make']);
        var edit_model = decodeURIComponent(o['model']);
        var edit_year = decodeURIComponent(o['year']);
        var edit_type = decodeURIComponent(o['type']);
        var edit_plate_nb = decodeURIComponent(o['platenb']);
        var edit_plate_code = decodeURIComponent(o['code']);
        var edit_stakeholders = decodeURIComponent(o['stakeholders']);
        var edit_type_name = decodeURIComponent(o['type_name']);
        var edit_year_name = decodeURIComponent(o['year_name']);
        var edit_code_name = decodeURIComponent(o['code_name']);
        var vehicle_index = decodeURIComponent(o['index']);

         
        // alert($("#vehicle_type option:selected").text()+"  "+edit_type);
         addDropDown('vehicle_type','Select Type','Select Type',vehicle_type_dropdown,edit_type_name);
         addDropDown('vehicle_year','Select Year','Select Year',vehicle_year_dropdown,edit_year_name);
         addDropDown('plate_code','Select Type','Select Type',plate_code_dropdown,edit_code_name);
         
         $.getScript("../js/sth-select.js", function(){});
         $("#car-model").val(edit_model);
         $("#car-make").val(edit_make);
         $("#vehicle_type").val(edit_type);
         $("#vehicle_year").val(edit_year);
         $("#car-plate").val(edit_plate_nb);
         $("#plate_code").val(edit_plate_code);
        // alert($("#vehicle_type option:selected").text());
        $("#car").css('display','none');
        $("#edit_car").css('display','block');

        //$('#stakeholders_list li').remove();
        var check_list = edit_stakeholders.split(',');
        for(var check_list_index = 0 ; check_list_index<check_list.length ; check_list_index++)
        {
            $("#checkbox_"+check_list[check_list_index]).attr('checked', true);
        }
    }




   // $("#stakeholders_list")
   //stakeholders_checkbox

    $("#car-model").on('focus',function(){
        $("#span-model").css({'color':'#e78200'});
    });

    $("#car-model").on('focusout',function(){
        $("#span-model").css({'color':'#000000'});
    });
   
    $("#car-make").on('focus',function(){
        $("#span-make").css({'color':'#e78200'});
    });

    $("#car-make").on('focusout',function(){
        $("#span-make").css({'color':'#000000'});
    });

    /*$("#car-year").on('focus',function(){
        $("#span-year").css({'color':'#e78200'});
    });

    $("#car-year").on('focusout',function(){
        $("#span-year").css({'color':'#000000'});
    });*/
   
    $("#car-plate").on('focus',function(){
        $("#span-plate").css({'color':'#e78200'});
    });

    $("#car-plate").on('focusout',function(){
        $("#span-plate").css({'color':'#000000'});
    });
   
  
    $("#car").on('click' ,function()
    { 
        

        var checked_ids = [];
        $.each(stakeholders_checkbox, function(index)
        {
            var val = $("#checkbox_"+stakeholders_checkbox[index]['id']).is(':checked');
            if(val == true)
            {
                checked_ids.push(stakeholders_checkbox[index]['id']);   
            }
            
        });
        
        var car_model = $("#car-model").val();
        var car_make = $("#car-make").val();
        var car_type = $("#vehicle_type").val();
        var car_year = $("#vehicle_year").val();
        var car_plate = $("#car-plate").val();
        var car_code = $("#plate_code").val();
        vehicle_id= (o['id']>0)?o['id']:'';
        var driver_id = getStorage("driver_id");
        vehicle_id = '';

        if(car_model == "" ||car_make == "" ||car_type == -1 ||car_year == -1 ||car_plate == "" ||car_code ==-1)
        {
            alert("Please Fill All Fields");
        }
        else if(checked_ids.join() =='')
        {
            alert('Please choose stakeholder(s)');
        }
        else
        {
            var params = "driver_id="+driver_id+"&car_model="+car_model+"&car_make="+car_make+"&car_type="
            +car_type+"&car_year="+car_year+"&car_plate="+car_plate+"&car_code="+car_code+"&checked_stakeholders="+checked_ids.join()+"&vehicle_id=";

            request(add_car_url,params,function(data)
            {
                if(data['success']==1)
                {
                    var current = document.getElementsByClassName("active");
                    current[0].className = current[0].className.replace(" active", "");
                    $("#menu_home").addClass('active');
                    $("#homeSubmenu").removeClass('in');
                    $("#navigate").attr('href','#/app/home');
                    var action_text = '';

                    if(vehicle_id == '')
                    {
                        car_info.push(new Array(data['idVehicle'],$("#vehicle_year :selected").text(),$("#vehicle_type :selected").text(),car_make,car_model,car_plate,$("#plate_code :selected").text(),$("#vehicle_year :selected").val(),$("#vehicle_type :selected").val(),$("#plate_code :selected").val()));
                        action_text = 'Added Successfully';
                    }
                    else
                    {
                        car_info[vehicle_index][0]=data['idVehicle'];
                        car_info[vehicle_index][1]=$("#vehicle_year :selected").text();
                        car_info[vehicle_index][2]=$("#vehicle_type :selected").text();
                        car_info[vehicle_index][3]=car_make;
                        car_info[vehicle_index][4]=car_model;
                        car_info[vehicle_index][5]=car_plate;
                        car_info[vehicle_index][6]=$("#plate_code :selected").text();
                        car_info[vehicle_index][7]=$("#vehicle_year :selected").val();
                        car_info[vehicle_index][8]=$("#vehicle_type :selected").val();
                        car_info[vehicle_index][9]=$("#plate_code :selected").val();


                        var temp = [];

                        $.each(vehicle_stakeholder_checkbox, function(index)
                        {
                            if(vehicle_stakeholder_checkbox[index][0]!=vehicle_id)
                                temp.push(new Array(vehicle_stakeholder_checkbox[index][0],vehicle_stakeholder_checkbox[index][1]));
        
                        });
                    
        
                        vehicle_stakeholder_checkbox = temp;

                        action_text = 'Edit Successfully';
                    }

                     
                    for(var checked_index = 0 ; checked_index<checked_ids.length;checked_index++)
                    {
                        vehicle_stakeholder_checkbox.push(new Array(data['idVehicle'] ,checked_ids[checked_index]));
                
                    }
                    if(vehicle_id == '')
                        $("#homeSubmenu").append('<li id="li_'+data['idVehicle']+'" onclick="getCarInfo('+data['idVehicle']+')" ><a  style="text-decoration : none; color : #000000;" href="#/app/carinfo?idVehicle='+data['idVehicle']+'"> <div id="list_'+data['idVehicle']+'" class="item item-button-right" style="border-top: none;border-bottom: none;font-size: 14px;font-weight: 500;" >'+car_model+" "+car_make+'</div></a></li>');
                    else
                        $("#list_"+vehicle_id).text(car_model+" "+car_make);
                    alertCallback(action_text,'',function()
                    {
                            $("#navigate").click();
                    });
                }
                else
                    alert("Error Occurred");
            });
        }
    });

    $("#edit_car").on('click',function()
    {
        status +='edit';

        var checked_ids = [];
        $.each(stakeholders_checkbox, function(index)
        {
            var val = $("#checkbox_"+stakeholders_checkbox[index]['id']).is(':checked');
            if(val == true)
            {
                checked_ids.push(stakeholders_checkbox[index]['id']);   
            }
            
        });
        
        var car_model = $("#car-model").val();
        var car_make = $("#car-make").val();
        var car_type = $("#vehicle_type").val();
        var car_year = $("#vehicle_year").val();
        var car_plate = $("#car-plate").val();
        var car_code = $("#plate_code").val();
        vehicle_id= (o['id']>0)?o['id']:'';
        var driver_id = getStorage("driver_id");


        if(car_model == "" ||car_make == "" ||car_type == -1 ||car_year == -1 ||car_plate == "" ||car_code ==-1)
        {
            alert("Please Fill All Fields");
        }
        else if(checked_ids.join() =='')
        {
            alert('Please choose stakeholder(s)');
        }
        else
        {
            var params = "driver_id="+driver_id+"&car_model="+car_model+"&car_make="+car_make+"&car_type="
            +car_type+"&car_year="+car_year+"&car_plate="+car_plate+"&car_code="+car_code+"&checked_stakeholders="+checked_ids.join()+"&vehicle_id="+vehicle_id;
            request(add_car_url,params,function(data)
            {
                if(data['success']==1)
                {
                    var current = document.getElementsByClassName("active");
                    current[0].className = current[0].className.replace(" active", "");
                    $("#menu_home").addClass('active');
                    $("#homeSubmenu").removeClass('in');
                    $("#navigate").attr('href','#/app/home');
                    var action_text = '';

                    if(vehicle_id == '')
                    {
                        car_info.push(new Array(data['idVehicle'],$("#vehicle_year :selected").text(),$("#vehicle_type :selected").text(),car_make,car_model,car_plate,$("#plate_code :selected").text(),$("#vehicle_year :selected").val(),$("#vehicle_type :selected").val(),$("#plate_code :selected").val()));
                        action_text = 'Added Successfully';
                    }
                    else
                    {
                        car_info[vehicle_index][0]=data['idVehicle'];
                        car_info[vehicle_index][1]=$("#vehicle_year :selected").text();
                        car_info[vehicle_index][2]=$("#vehicle_type :selected").text();
                        car_info[vehicle_index][3]=car_make;
                        car_info[vehicle_index][4]=car_model;
                        car_info[vehicle_index][5]=car_plate;
                        car_info[vehicle_index][6]=$("#plate_code :selected").text();
                        car_info[vehicle_index][7]=$("#vehicle_year :selected").val();
                        car_info[vehicle_index][8]=$("#vehicle_type :selected").val();
                        car_info[vehicle_index][9]=$("#plate_code :selected").val();


                        var temp = [];

                        $.each(vehicle_stakeholder_checkbox, function(index)
                        {
                            if(vehicle_stakeholder_checkbox[index][0]!=vehicle_id)
                                temp.push(new Array(vehicle_stakeholder_checkbox[index][0],vehicle_stakeholder_checkbox[index][1]));
        
                        });
                    
        
                        vehicle_stakeholder_checkbox = temp;

                        action_text = 'Edit Successfully';
                    }

                     
                    for(var checked_index = 0 ; checked_index<checked_ids.length;checked_index++)
                    {
                        vehicle_stakeholder_checkbox.push(new Array(data['idVehicle'] ,checked_ids[checked_index]));
                
                    }
                    if(vehicle_id == '')
                        $("#homeSubmenu").append('<li id="li_'+data['idVehicle']+'" onclick="getCarInfo('+data['idVehicle']+')" ><a  style="text-decoration : none; color : #000000;" href="#/app/carinfo?idVehicle='+data['idVehicle']+'"> <div id="list_'+data['idVehicle']+'" class="item item-button-right" style="border-top: none;border-bottom: none;font-size: 14px;font-weight: 500;" >'+car_model+" "+car_make+'</div></a></li>');
                    else
                        $("#list_"+vehicle_id).text(car_model+" "+car_make);
                    alertCallback(action_text,'',function()
                    {
                            $("#navigate").click();
                    });
                }
                else
                    alert("Error Occurred");
            });
        }
    });
})

function s()
{
    $('li > a > div').removeClass('submenu_active');
    
   vehicle_id='';
    
    var car_model = $("#car-model").val('');
    var car_make = $("#car-make").val('');
    var car_plate = $("#car-plate").val('');
    
    
    $("#content_vehicle_type").html('');
    $("#content_vehicle_year").html('');
    $("#content_plate_code").html('');
    addDropDown('vehicle_type','Select Type','Select Type',vehicle_type_dropdown);
    addDropDown('vehicle_year','Select Year','Select Year',vehicle_year_dropdown);
    addDropDown('plate_code','Select Type','Select Type',plate_code_dropdown);
    $.getScript("../js/sth-select.js", function(){});

    $.each(stakeholders_checkbox, function(index)
    {
        $("#checkbox_"+stakeholders_checkbox[index]['id']).attr('checked', false);
    });


    $("#car").css('display','block');
    $("#edit_car").css('display','none');
    
    $("#car_action").text('Add Car');

    
}