angular.module('police_sidemenu', ['police_sidemenu.controllers' , 'ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    
  
   
    $ionicPlatform.registerBackButtonAction(function () 
    {
        //history.go(-(history.length - 1));
        //window.history.go(-4); 
        ionic.Platform.exitApp();

    }, 100);

  });
  
   $ionicPlatform.registerBackButtonAction(function (event) {
      if($state.current.name=="app.home"){
        navigator.app.exitApp(); //<-- remove this line to disable the exit
      }
      else {
        navigator.app.backHistory();
      }
    }, 100);

   
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: '../police/menu.html',
    controller: 'police_menuCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: '../police/home.html',
        controller: 'police_homeCtrl'
      }
    }
  })

.state('app.profile', {
  url: '/profile',
  views: {
    'menuContent': {
      templateUrl: '../police/police_profile.html',
      controller: 'police_profileCtrl'
    }
  }
})
.state('app.notification', {
  url: '/notification',
  views: {
    'menuContent': {
      templateUrl: '../police/police_notification.html',
      controller: 'police_notificationCtrl'
    }
  }
})
.state('app.notification_list', {
  url: '/notification_list',
  views: {
    'menuContent': {
      templateUrl: '../police/police_notification_list.html',
      controller: 'police_notification_listCtrl'
    }
  }
})
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});