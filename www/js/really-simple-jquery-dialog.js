/**
 * jQuery Really Simple Dialog plugin 1.0
 *
 * Copyright (c) 2017 NTD3004 (https://github.com/NTD3004/JQuery-Really-Simple-Dialog)
 *
 * Samples and instructions at: 
 * https://github.com/NTD3004/JQuery-Really-Simple-Dialog
 *
 * This script is free software: you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 */

(function($) {
	$.fn.simpleAlert = function(options) {
		var c =0;
		if (typeof options === 'undefined') options = {};
		
		var defaultOptions = {
	        title: 'Alert',
	        message: '',
			btnLabel: 'OK'
	    }
	    options = $.extend(defaultOptions, options);

	    this.each(function() {
	    	var $this = $(this);
	    	var html;

	    	$this.addClass('simple-dialog active');

	    	html = '<div class="simple-dialog-content">';
	    	html += '<div class="simple-dialog-header"><h3 class="title">'+options.title+'</h3></div>';
	    	html += '<div class="simple-dialog-body"><p class="message">'+options.message+'</p></div>';
	    	html += '<div class="simple-dialog-footer"><span id="alert-ok" class="simple-dialog-button" data-action="close">'+options.btnLabel+'</span></div>';
	    	html += '</div>';

			$this.html(html);
			var p= options.callback;

	    	$(document).on('click', 'a[data-action="close"] , #alert-ok', function(e) {
				e.preventDefault();
				$(this).parents('.simple-dialog').removeClass('active');
					
			});
	    });

	    return this;
	};

	$.fn.simpleConfirm = function(options) {
		if (typeof options === 'undefined') options = {};

        var defaultOptions = {
	        title: 'Confirm',
	        message: '',
	        acceptBtnLabel: 'Ok',
	        cancelBtnLabel: 'Cancel',
	        success: function() {},
	        cancel: function() {}
	    }
	    options = $.extend(defaultOptions, options);

	    this.each(function() {
	    	var $this = $(this);
	    	var html;

	    	$this.addClass('simple-dialog active');

	    	html = '<div class="simple-dialog-content">';
	    	html += '<div class="simple-dialog-header"><h3 class="title">'+options.title+'</h3></div>';
	    	html += '<div class="simple-dialog-body"><p class="message">'+options.message+'</p></div>';
	    	html += '<div class="simple-dialog-footer clearfix"><span id="confirm-accept" class="simple-dialog-button accept" style="margin-right:10px;">'+options.acceptBtnLabel+'</span><span  id="confirm-cancel" class="simple-dialog-button cancel">'+options.cancelBtnLabel+'</span></div>';
	    	html += '</div>';

	    	$this.html(html);

	    	$(document).on('click', 'a[data-action="close"] ,#confirm-accept , #confirm-cancel', function(e) {
				e.preventDefault();
				$(this).parents('.simple-dialog').removeClass('active');
				if($(this).hasClass('accept')) {
					options.success();
				}
				if($(this).hasClass('cancel')) {
					options.cancel();
				}
			});

	    });

	    return this;
	};

	$.fn.simpleAlertCallback = function(options) {
		if (typeof options === 'undefined') options = {};

        var defaultOptions = {
	        title: 'Confirm',
	        message: '',
	        acceptBtnLabel: 'Ok',
	        cancelBtnLabel: 'Cancel',
	        success: function() {},
	        cancel: function() {}
	    }
	    options = $.extend(defaultOptions, options);

	    this.each(function() {
	    	var $this = $(this);
	    	var html;

	    	$this.addClass('simple-dialog active');

	    	html = '<div class="simple-dialog-content">';
	    	html += '<div class="simple-dialog-header"><h3 class="title">'+options.title+'</h3></div>';
	    	html += '<div class="simple-dialog-body"><p class="message">'+options.message+'</p></div>';
			html += '<div class="simple-dialog-footer"><span id="confirm-accept" class="simple-dialog-button" style="margin-right:10px;">'+options.acceptBtnLabel+'</span></div>';
	    
			html += '</div>';

	    	$this.html(html);

	    	$this.html(html);

	    	$(document).on('click', 'a[data-action="close"] ,#confirm-accept', function(e) {
				e.preventDefault();
				$(this).parents('.simple-dialog').removeClass('active');
				
					options.success();
				
				if($(this).hasClass('cancel')) {
					options.cancel();
				}
			});

	    });

	    return this;
	};

	$.fn.simplePrompt = function(options) {
		var c =0;
		if (typeof options === 'undefined') options = {};

        var defaultOptions = {
	        title: 'Prompt',
			message: '',
			type:'text',
			value:'',
	        acceptBtnLabel: 'Ok',
	        cancelBtnLabel: 'Cancel',
	        success: function(result) {},
	        cancel: function(result) {}
	    }
	    options = $.extend(defaultOptions, options);

	    this.each(function() {
	    	var $this = $(this);
	    	var html;

	    	$this.addClass('simple-dialog active');

	    	html = '<div class="simple-dialog-content">';
	    	html += '<div class="simple-dialog-header"><h3 class="title">'+options.title+'</h3></div>';
	    	html += '<div class="simple-dialog-body"><p class="message">'+options.message+'</p><p class="answer"> <label class="item item-input"><input id="profile_input" type="'+options.type+'"  value="'+options.value+'"/></label></p></div>';
	    	html += '<div class="simple-dialog-footer clearfix"><span id="prompt_accept" class="simple-dialog-button accept" data-action="close" style="margin-right:1px;">'+options.acceptBtnLabel+'</span><span id="prompt_cancel" class="simple-dialog-button cancel" data-action="close">'+options.cancelBtnLabel+'</span></div>';
	    	html += '</div>';

	    	$this.html(html);

	    	$(document).on('click', 'a[data-action="close"] ,#prompt_accept ,#prompt_cancel', function(e) {
				if(c<1)
				{
				c++;
				e.preventDefault();
				var result = $('input').val();
				$(this).parents('.simple-dialog').removeClass('active');
				if($(this).hasClass('accept')) {
					options.success($("#profile_input").val());
				}
				if($(this).hasClass('cancel')) {
					options.cancel(result);
				}}
			});
	    });

	    return this;
	};

	$.fn.checkboxPrompt = function(options) 
	{
		var c =0;
		if (typeof options === 'undefined') options = {};

        var defaultOptions = {
	        title: 'Prompt',
			message: '',
			type:'text',
			value:'',
	        acceptBtnLabel: 'Ok',
	        cancelBtnLabel: 'Cancel',
	        success: function(result) {},
	        cancel: function(result) {}
	    }
	    options = $.extend(defaultOptions, options);

	    this.each(function() {
	    	var $this = $(this);
	    	var html;

	    	$this.addClass('simple-dialog active');

	    	html = '<div class="simple-dialog-content">';
	    	html += '<div class="simple-dialog-header"><h3 class="title">'+options.title+'</h3></div>';
			
			

			
			
			html+= '<ul style="margin:10px;padding:10px;" id="accident_stakeholder_list">';


			$.each(stakeholders_checkbox, function(index)
			{
				html+= '<li class = "item item-checkbox checkbox-energized" style="border:none;">';
				html+= stakeholders_checkbox[index]['value'];
				html+= '<label class = "checkbox" style="border:0;">';
				html += '<input type = "checkbox" value="'+stakeholders_checkbox[index]['id']+'" id="'+'accident_stakeholder_'+stakeholders_checkbox[index]['id']+'" style="margin-left:0px; border:0;" />';
				html+= ' </label>';   
				html+= '</li>';

			});
			
			html+='</ul>';
			



					
			html += '<div class="simple-dialog-footer clearfix"><span id="prompt_accept" class="simple-dialog-button accept" data-action="close" style="margin-right:1px;">'+options.acceptBtnLabel+'</span><span id="prompt_cancel" class="simple-dialog-button cancel" data-action="close">'+options.cancelBtnLabel+'</span></div>';
	    	html += '</div>';

	    	$this.html(html);

	    	$(document).on('click', 'a[data-action="close"] ,#prompt_accept ,#prompt_cancel', function(e) {
				if(c<1)
				{
				c++;
				e.preventDefault();
				var result = $('input').val();
				$(this).parents('.simple-dialog').removeClass('active');
				if($(this).hasClass('accept')) 
				{
					var accident_stakeholder_checked = [];
					
					$.each(stakeholders_checkbox, function(index)
					{
						 if($('#accident_stakeholder_'+stakeholders_checkbox[index]['id']).is(":checked")==true)
						 {
						 	accident_stakeholder_checked.push($('#accident_stakeholder_'+stakeholders_checkbox[index]['id']).val());
						 }
					
					});
					options.success(accident_stakeholder_checked);
				}
				if($(this).hasClass('cancel')) {
					options.cancel(result);
				}}
			});
	    });

	    return this;
	};

	$.fn.carlistprompt = function(options) 
	{
		var c =0;
		if (typeof options === 'undefined') options = {};

        var defaultOptions = {
	        title: 'Prompt',
			message: '',
			type:'text',
			value:'',
	        acceptBtnLabel: 'Ok',
	        cancelBtnLabel: 'Cancel',
	        success: function(result) {},
	        cancel: function(result) {}
	    }
	    options = $.extend(defaultOptions, options);

	    this.each(function() {
	    	var $this = $(this);
	    	var html;

	    	$this.addClass('simple-dialog active');

	    	html = '<div class="simple-dialog-content">';
	    	html += '<div class="simple-dialog-header"><h3 class="title">'+options.title+'</h3></div>';
			html += '<div class="simple-dialog-body">';
			
			html+='<div class="list">';
			
			$.each(car_info, function(index) 
            {
				if(car_info[index].length>0)
				{
					html+='<label class="item item-radio">';
					html+='<input type="radio" name="group" value="'+car_info[index][0]+'">';
					html+='<div class="radio-content">';
					html+='<div class="item-content">';
					html+=car_info[index][4]+"  "+car_info[index][3];
					html +='</div>';
					html +='<i class="radio-icon ion-checkmark" style="color:#e78200"></i>';
					html+='</div>';
					html+='</label>';
				}

			});


			html+='</div>';
			html+='</div>';


					
			html += '<div class="simple-dialog-footer clearfix"><span id="prompt_accept" class="simple-dialog-button accept" data-action="close" style="margin-right:1px;">'+options.acceptBtnLabel+'</span><span id="prompt_cancel" class="simple-dialog-button cancel" data-action="close">'+options.cancelBtnLabel+'</span></div>';
	    	html += '</div>';

	    	$this.html(html);

	    	$(document).on('click', 'a[data-action="close"] ,#prompt_accept ,#prompt_cancel', function(e) {
				if(c<1)
				{
				c++;
				e.preventDefault();
				var result = $('input').val();
				$(this).parents('.simple-dialog').removeClass('active');
				if($(this).hasClass('accept')) 
				{
					var value=[];
					value.push($('input[name=group]:checked').val());
					value.push($('input[name=group]:checked').parent('label').text());
					options.success(value);
				}
				if($(this).hasClass('cancel')) {
					options.cancel(result);
				}}
			});
	    });

	    return this;
	};

})(jQuery);