var days = [];
angular.module('sidemenu.controllers')
.controller('statisticsCtrl', function($scope)
{ 
    $('li > a > div').removeClass('submenu_active');   
   
   $.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyAXX46Kf8xIz7ZPaZf8VABw3WPYUq0l8tg',function(){
    var map = new google.maps.Map(document.getElementById('map'), 
    {
      zoom: 8,
      center: {lat: 33.6149908, lng: 35.8464298}
    });

    $.each(clustering_lats,function(index)
    {
      var myLatlng = new google.maps.LatLng(clustering_lats[index],clustering_lngs[index]);
      var marker = new google.maps.Marker(
      {
      position: myLatlng
      });

      marker.setMap(map);
    })
   })

    /*var map = new google.maps.Map(document.getElementById('map'), 
    {
      zoom: 8,
      center: {lat: 33.6149908, lng: 35.8464298}
    });

    $.each(clustering_lats,function(index)
    {
      var myLatlng = new google.maps.LatLng(clustering_lats[index],clustering_lngs[index]);
      var marker = new google.maps.Marker(
      {
      position: myLatlng
      });

      marker.setMap(map);
    })
   */

    
    request(get_statistics_url,'',function(data)
    {
      days=[];
      lights=[];
      $.each(data['days_stat'],function(index)
      {
          days.push([(data['days_stat'][index].name).substring(0,3),(data['days_stat'][index].total*100)/data['total']]);  
      })

//////////////////////////////////////////////////////////////////////////////////////////////////////////
      var lightData = [];
      var light_stats_colors=['#083b66','#e78200'];
      

      $.each(data['light_stat'],function(index)
      {
         lightData.push({label:data['light_stat'][index].name,data:(data['light_stat'][index].total*100)/data['total'],color:light_stats_colors[index] });
      })

      var bar_data = {
        data : days,
        color: '#3c8dbc'
      }
      $.plot('#bar-chart', [bar_data], {
        grid  : {
          borderWidth: 1,
          borderColor: '#f3f3f3',
          tickColor  : '#f3f3f3'
        },
        series: {
          bars: {
            show    : true,
            barWidth: 0.5,
            align   : 'center'
          }
        },
        xaxis : {
          mode      : 'categories',
          tickLength: 0
        }
      })

      $.plot('#donut-chart', lightData, {
        series: {
          pie: {
            show       : true,
            radius     : 1,
            innerRadius: 0.5,
            label      : {
              show     : true,
              radius   : 2 / 3,
              formatter: labelFormatter,
              threshold: 0.01
            }
  
          }
        },
        legend: {
          show: false
        }
      })
//////////////////////////////////////////////////////////////////////////////////////////////////////////
 
var btw_0_18=(data['btw_0_18']*100)/data['total'];
var btw_19_30 = (data['btw_19_30']*100)/data['total'];
var btw_31_60 = (data['btw_31_60']*100)/data['total'];
var greater_60 = 100-(btw_0_18+btw_19_30+btw_31_60);

var age_stats_colors=['#083b66','#e78200','#3c8dbc','#cc0000'];

var ageData=
[
  {label: '0-18', data: btw_0_18, color: age_stats_colors[0]},
  {label: '19-30', data: btw_19_30, color: age_stats_colors[1]},
  {label: '31-60', data: btw_31_60, color: age_stats_colors[2]},
  {label: '>60', data: greater_60, color: age_stats_colors[3]}
]

$.plot('#donut-chart2', ageData, {
  series: {
    pie: {
      show       : true,
      radius     : 1,
      innerRadius: 0.5,
      label      : {
        show     : true,
        radius   : 2 / 3,
        formatter: labelFormatter,
        threshold: 0.01
      }

    }
  },
  legend: {
    show: false
  }
})

    })

      
  
    function labelFormatter(label, series) 
    {
      return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
        + label
        + '<br>'
        + Math.round(series.percent) + '%</div>'
    }
   
});