var coverage_id = [];
angular.module('insurance_sidemenu.controllers')
.controller('driver_coverageCtrl', function($scope) 
{ 
   
    addDropDown('user','Select Driver','Select Driver',insurance_driver_dropdown);
    addDropDown('policy_dropdowns','Select Policy','Select Policy',insurance_policy_dropdown);
    
    $.getScript("../js/sth-select.js", function(){});
  
$("#list_vehicle").on('click',function()
{
    var selected_value = $("#user").val();

    if(selected_value==-1)
        alert('Please Select Driver');
    else
    {
        //get_vehicle_coverage_url
        var params = 'driver_id='+selected_value;
        request(get_driver_coverage_url,params,function(data)
        {
            $("#vehicle_list").html('');
            if(data['success']==-1)
            {
                var content = '<div>';
                content +='<img src="../img/notification.svg" style="width: 100px;height: 100px;margin: auto; display: block;margin-top:10px;"/>';
                content +='<p style="text-align: center;color:#083b66;">No Notifications</p>';
                content +='</div>';
                $("#vehicle_list").html(content);
            }
            else
            {
                if(data['success']==1)
                {
                    coverage_id=[];
                    var content = ' <div class="item item-divider" style="background-color: #083b66;color:#FFFFFF;">Policy List</div>';
                    $("#vehicle_list").append(content);

                    var content = '<ul class="list">';

                    $.each(data['driver_coverage'], function(index) 
                    {
                        coverage_id.push(data['driver_coverage'][index].idPolicy_coverage);
                        content +='<li class="item item-toggle">';
                        content +='Policy Number: '+data['driver_coverage'][index].PolicyNumber;
                        content +='<label class="toggle toggle-energized">';
                        
                        if(data['driver_coverage'][index].coverage_active==1)
                            content+='<input type="checkbox" checked id="checked_'+data['driver_coverage'][index].idPolicy_coverage+'" >';
                        else
                            content+='<input type="checkbox" id="checked_'+data['driver_coverage'][index].idPolicy_coverage+'" >';

                        content+='<div class="track"><div class="handle"></div>';
                        content +='</div></label></li>';

                    }); 
                    content +='</ul>';  
                    content += '<button onclick="save_changes()" class="button login-button icon-right ion-android-done" style="background-color: #083b66; border-color: #e78200;color: #FFFFFF;margin-bottom: 5px; border-radius:5px;"  id="coverage_save_changes">Save Changes</button> ';
                    $("#vehicle_list").append(content);

                }
                else
                {
                    alert('An Error Occurred');
                }
            }
        })
    }    
})

$("#add_policy").on('click',function()
{
    //add_vehicle_coverage_url
    var selected_value = $("#user").val();
    var selected_policy = $("#policy_dropdowns").val();

    if(selected_value == -1 || selected_policy==-1)
    {
        alert('Please Select Driver And Policy');
    } 
    else
    {
        var params = "driver_id="+selected_value+"&policy_id="+selected_policy+"&action=1&checked=&coverage_id=";
        request(add_driver_coverage_url,params,function(data)
        {
            if(data['success']==1)
            {
                alertCallback('Data Added Successfully','',function()
                {
                    $("#menu_driver_coverage").removeClass('active');
                    $("#menu_policy").addClass('active');
                    $("#coverage_navigate").attr('href','#/app/policy');
                    $("#coverage_navigate").click();
                })
            }
        })
    }
})


    
});

function save_changes()
{
//($("#checekd_").is(':checked')==false)?0:1;
var checked = [];
for(var index=0;index<coverage_id.length;index++)
{
    checked.push(($("#checked_"+coverage_id[index]).is(':checked')==false)?0:1);
}

var params="driver_id=&policy_id=&action=2&checked="+checked.join()+"&coverage_id="+coverage_id.join();
request(add_driver_coverage_url,params,function(data)
{
    if(data['success']==1)
    {
        alertCallback('Data Updated Successfully','',function()
        {
            $("#coverage_navigate").attr('href','#/app/policy');
            $("#coverage_navigate").click();
        })
    }
    else
    {
        alert('An Error Occurred');
    }
})
}