//cordova plugin add cordova-sqlite-storage --save
var myDB;


function createDb(db_name)
{
     myDB = window.sqlitePlugin.openDatabase({name: db_name, location: 'default'});
}

function createTable(table_name , attributes)
{
    var counter = 0;
    var table="CREATE TABLE IF NOT EXISTS "+table_name+" (";
    
    for (var key in attributes) 
    {
        table += " "+key+" "+attributes[key];
        table+=(counter == Object.keys(attributes).length-1)?")":",";
        counter++;
    }
    
    
        myDB.transaction(function(transaction) 
        {
            transaction.executeSql(table, [],
            function(tx, result) {
            alert("Table created successfully");
            },
            function(error) {
            alert("Error occurred while creating the table.");
            });
        });
}

function sqlite_query(query)
{
    var title="sundaravel";
    var desc="phonegap freelancer";
    myDB.transaction(function(transaction) {
    var executeQuery = query;
    transaction.executeSql(executeQuery, []
    , function(tx, result) {
    alert('Inserted');
    },
    function(error){
        alert(error);
    });
    
    });
}

function sqlite_display(query)
{
  
    myDB.transaction(function(transaction) {
        transaction.executeSql(query, [], function (tx, results) {
        var len = results.rows.length, i;
        $("#rowCount").append(len);
        for (i = 0; i < len; i++)
        {
            $("#TableData").append("<tr><td>"+results.rows.item(i).id+"</td><td>    "+results.rows.item(i).title+"</td><td> "+results.rows.item(i).desc+"</td></tr>");
           
        }
        }, function(error){
            alert(error);
        });
        });
}

function sqlite_tables(query)
{
    myDB.transaction(function(transaction) {
        transaction.executeSql(query, [], function (tx, results) {
        var len = results.rows.length, i;
        $("#rowCount").append(len);
        for (i = 0; i < len; i++)
        {
            $("#TableData").append("<tr><td>"+results.rows.item(i).name+"</td></tr>");
            
        }
        }, function(error){
            alert();
        });
        });
}

function sqlite_fields(query)
{
    myDB.transaction(function(transaction) {
        transaction.executeSql(query, [], function (tx, results) {
        var len = results.rows.length, i;
        $("#rowCount").append(len);
        for (i = 0; i < len; i++)
        {
            $("#TableData").append("<tr><td>"+results.rows.item(i).sql+"</td></tr>");
            
        }
        }, function(error){
            alert();
        });
        });
}