angular.module('insurance_sidemenu.controllers')
.controller('policyCtrl', function($scope) 
{
    //policy_content
    /*var content = '<div class="card">';
    content += '<div class="item item-text-wrap">';
    content += '<div class="row">';
    content += '<div class="col">';
    content += '<b>Policy Number</b>';
    content += '<p>ssss</p>';
    content += '</div>';
    content += '<div class="col">';
    content += '<b>Policy Number</b>';
    content += '<p>ssss</p>';
    content += '</div>';
    content += '</div>';
    content += '</div>';
    content += '</div>';

    $("#policy_content").append(content);*/


    request(get_policy_url,'',function(data)
    {
        $.each(data['policy'], function(index) 
        {
            var content = '<div class="card">';
            content += '<div class="item item-text-wrap">';

            content += '<div class="row">';
            content += '<div class="col">';
            content += '<b>Policy Number</b>';
            content += '<p>'+data['policy'][index].PolicyNumber+'</p>';
            content += '</div>';
            content += '<div class="col">';
            content += '<b>Effective Date</b>';
            content += '<p>'+data['policy'][index].PolicyEffectiveDate+'</p>';
            content += '</div>';
            content += '</div>';
            
            content += '<div class="row">';
            content += '<div class="col">';
            content += '<b>Expiry Date</b>';
            content += '<p>'+data['policy'][index].PolicyExpiryDate+'</p>';
            content += '</div>';
            content += '<div class="col">';
            content += '<b>Payment Option</b>';
            content += '<p>'+data['policy'][index].PaymentOption+'</p>';
            content += '</div>';
            content += '</div>';

            content += '<div class="row">';
            content += '<div class="col">';
            content += '<b>Total Amount($)</b>';
            content += '<p>'+data['policy'][index].TotalAmount+'</p>';
            content += '</div>';
            content += '<div class="col">';
            content += '<b>Additional Info</b>';
            content += '<p>'+(data['policy'][index].additionalinfo==null)?'':data['policy'][index].additionalinfo+'</p>';
            content += '</div>';
            content += '</div>';

            content += '</div>';
            content += '</div>';
        
            $("#policy_content").append(content);
        });
    })
  
});