var coverage_id = [];
angular.module('insurance_sidemenu.controllers')
.controller('vehicle_coverageCtrl', function($scope) 
{
 

    addDropDown('vehicles','Select Vehicle','Select Vehicle',insurance_vehicle_dropdown);
    addDropDown('policy_dropdown','Select Policy','Select Policy',insurance_policy_dropdown);
    $.getScript("../js/sth-select.js", function(){});

	function centralize(){
				var $container = $(".container");
				var containerHeight = $container.outerHeight();
				var windowHeight = $(window).outerHeight();
				var marginTop = (windowHeight / 2) - (containerHeight / 2);
				var MAX_MARGIN_TOP = 230;

				marginTop = (marginTop > MAX_MARGIN_TOP) ? MAX_MARGIN_TOP : marginTop;

				$container.css({"margin-top": marginTop + "px"});
			}

			centralize();
            $(window).resize(centralize);
            
		
    $("#list_vehicle").on('click',function()
    {
        var selected_value = $("#vehicles").val();

        if(selected_value==-1)
            alert('Please Select Vehicle');
        else
        {
            //get_vehicle_coverage_url
            var params = 'vehicle_id='+selected_value;
            request(get_vehicle_coverage_url,params,function(data)
            {
                $("#vehicle_list").html('');
                if(data['success']==-1)
                {
                    var content = '<div>';
                    content +='<img src="../img/notification.svg" style="width: 100px;height: 100px;margin: auto; display: block;margin-top:10px;"/>';
                    content +='<p style="text-align: center;color:#083b66;">No Notifications</p>';
                    content +='</div>';
                    $("#vehicle_list").html(content);
                }
                else
                {
                    if(data['success']==1)
                    {
                        coverage_id=[];
                        var content = ' <div class="item item-divider" style="background-color: #083b66;color:#FFFFFF;">Policy List</div>';
                        $("#vehicle_list").append(content);

                        var content = '<ul class="list">';

                        $.each(data['vehicle_coverage'], function(index) 
                        {
                            coverage_id.push(data['vehicle_coverage'][index].idVehicle_coverage);
                            content +='<li class="item item-toggle">';
                            content +='Policy Number: '+data['vehicle_coverage'][index].PolicyNumber;
                            content +='<label class="toggle toggle-energized">';
                            
                            if(data['vehicle_coverage'][index].coverage_active==1)
                                content+='<input type="checkbox" checked id="checked_'+data['vehicle_coverage'][index].idVehicle_coverage+'" >';
                            else
                                content+='<input type="checkbox" id="checked_'+data['vehicle_coverage'][index].idVehicle_coverage+'" >';

                            content+='<div class="track"><div class="handle"></div>';
                            content +='</div></label></li>';

                        }); 
                        content +='</ul>';  
                        content += '<button onclick="save_changes()" class="button login-button icon-right ion-android-done" style="background-color: #083b66; border-color: #e78200;color: #FFFFFF;margin-bottom: 5px; border-radius:5px;"  id="coverage_save_changes">Save Changes</button> ';
                        $("#vehicle_list").append(content);

                    }
                    else
                    {
                        alert('An Error Occurred');
                    }
                }
            })
        }    
    })

    $("#add_policy").on('click',function()
    {
        //add_vehicle_coverage_url
        var selected_value = $("#vehicles").val();
        var selected_policy = $("#policy_dropdown").val();

        if(selected_value == -1 || selected_policy==-1)
        {
            alert('Please Select Vehicle And Policy');
        } 
        else
        {
            var params = "vehicle_id="+selected_value+"&policy_id="+selected_policy+"&action=1&checked=&coverage_id=";
            request(add_vehicle_coverage_url,params,function(data)
            {
                if(data['success']==1)
                {
                    alertCallback('Data Added Successfully','',function()
                    {
                        $("#menu_vehicle_coverage").removeClass('active');
                        $("#menu_policy").addClass('active');
                        $("#coverage_navigate").attr('href','#/app/policy');
                        $("#coverage_navigate").click();
                    })
                }
            })
        }
    })

    
        
});

function save_changes()
{
    //($("#checekd_").is(':checked')==false)?0:1;
    var checked = [];
    for(var index=0;index<coverage_id.length;index++)
    {
        checked.push(($("#checked_"+coverage_id[index]).is(':checked')==false)?0:1);
    }

    var params="vehicle_id=&policy_id=&action=2&checked="+checked.join()+"&coverage_id="+coverage_id.join();
    request(add_vehicle_coverage_url,params,function(data)
    {
        if(data['success']==1)
        {
            alertCallback('Data Updated Successfully','',function()
            {
                $("#coverage_navigate").attr('href','#/app/policy');
                $("#coverage_navigate").click();
            })
        }
        else
        {
            alert('An Error Occurred');
        }
    })
}


function e()
{
    alert('s');
}