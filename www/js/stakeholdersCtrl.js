angular.module('sidemenu.controllers')
.controller('stakeholdersCtrl', function($scope) 
{
    vehicles_id = [];
    $('li > a > div').removeClass('submenu_active');  
    
    var stakeholders_icons = ['../img/ambulance.svg','../img/police.svg','../img/insurance.svg'];
    
    $.each(stakeholders_checkbox, function(index)
    {
        var content = '<span class="item item-icon-left  item-icon-right">';
        if(stakeholders_icons[index] == '')
             content += '<i class="icon ion-chatbubble-working"></i>';
        else
             content += '<img src="'+stakeholders_icons[index]+'" class="icon ion-chatbubble-working"/>';

        content += '<p style="text-align:center;"><b>'+stakeholders_checkbox[index]['value']+'</b></p>';
        content += '<i class="icon ion-ios-telephone-outline" onclick="callPnNumber('+stakeholders_checkbox[index]['stakeholder_pnumber']+');"></i>';
        content += '</span>';

        $("#stakeholder_list").append(content);
                    
    });   

    /*
    var counter = 0;

    $.each(car_info, function(index)
    {
        if(car_info[index].length>0)
        {
            vehicles_id.push(car_info[index][0]);
            counter ++ ;
            var content = '<label class="item item-input item-stacked-label" style="border-bottom:none;">';
            content += '<span class="input-label"><span id="span-plate" class="ion-model-s"></span>';
            content += '<b> '+car_info[index][4] + " " + car_info[index][3]+'</b>';
            content += '</span>';
            content += '</label>';
            content +='<ul class = "list" style="border: 0;" id="vehicle_stakeholders_'+car_info[index][0]+'"> ';
           
            $.each(stakeholders_checkbox, function(i)
            {
                content += '<li class = "item item-checkbox checkbox-energized">';
                content += stakeholders_checkbox[i]['value'];
                content += '<label class = "checkbox">';
                content += '<input type = "checkbox" style="margin-left:0px; border:0;" id="vehicle_stakeholders_'+car_info[index][0]+"_"+stakeholders_checkbox[i]['id']+'" />';
                content += '</label>';
                content += '</li>';
                $("#vehicle_stakeholders_"+car_info[index][0]).append(content);

            });  

            content += '</ul>';
            $("#vehicle_stakeholders_content").append(content);
        }
    });

    if(counter ==0)
    {
        $("#no_cars").css('display','block');
        $("#save_changes").css('display','none');
    }
    else
    {
        $("#no_cars").css('display','none');
        $("#save_changes").css('display','block');
  


    request(getVehiclesStakeholders_link,'vehicle_ids='+vehicles_id,function(data)
    {
        //alert(car_info.length+" "+data['vehicle-stakeholders_success']);
       if(data['vehicle-stakeholders_success'] == 1)
        {
            if(data['vehicle_stakeholders'].length>0)
            {
                $.each(data['vehicle_stakeholders'], function(index)
                {
                    // alert(data['vehicle_stakeholders'].vehicle_idvehicle);
                    $("#vehicle_stakeholders_"+data['vehicle_stakeholders'][index].vehicle_idvehicle+"_"+data['vehicle_stakeholders'][index].stakeholder_idstakeholder).attr('checked', true);
                    //alert(vehicle_stakeholder_checkbox[index].idVehicleStakeholders);
                    
                }); 
            }
            //alert(data['vehicle_stakeholders'][index].vehicle_idvehicle);
        }
        else if(data['vehicle-stakeholders_success'] == 0)
        {
            alert('An Error Occurred');
        }
    });
   
}




    $("#save_changes").on('click',function()
    {
            vehicles_id = [];
            var test = '';
        edited_checkbox_stakeholders = [];
         
         $.each(car_info, function(index)
         {
            
             if(car_info[index].length>0)
             {
                vehicles_id.push(car_info[index][0]);
                 $.each(stakeholders_checkbox, function(i)
                 {
                     if($("#vehicle_stakeholders_"+car_info[index][0]+"_"+stakeholders_checkbox[i]['id']).is(":checked") ==true)
                     {
                         
                         edited_checkbox_stakeholders.push(car_info[index][0]+"_"+stakeholders_checkbox[i]['id']);      
                     }
                 
                 });  
                
             }
         });
         //alert(car_info.length +" "+vehicles_id);
         var params = "vehicle_stakeholders="+edited_checkbox_stakeholders+"&vehicles_id="+vehicles_id;
         
         request(vehicle_stakeholders_edit_link,params,function(data)
         {
             if(data['success'] == 1)
             {
                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace(" active", "");
                $("#menu_home").addClass('active');
                $("#homeSubmenu").removeClass('in');
                $("#navigate").attr('href','#/app/home');
                alertCallback('Stakeholder Edit Successfully','',function()
                {
                     $("#navigate").click();
                });

             }
             else
             {
                 alert("An Error Occurred");
             }
            //prompt(data['success'],data['success'],'text',data['success']);

         });  



    });*/
})
