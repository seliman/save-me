angular.module('sidemenu.controllers')

.controller('profileCtrl', function($scope) 
{
    $('li > a > div').removeClass('submenu_active');
    
    var photo_canged = 0;
    var new_password = '';
    var user_id = getStorage("user_id");
    var driver_id=getStorage("driver_id");
    var first_name = (driver_info_defines[0]=='' || driver_info_defines[0]==null || driver_info_defines[0]=='null')?'':driver_info_defines[0];
    var middle_name = (driver_info_defines[1]=='' || driver_info_defines[1]==null || driver_info_defines[1]=='null')?'':driver_info_defines[1];
    var last_name = (driver_info_defines[2]=='' || driver_info_defines[2]==null || driver_info_defines[2]=='null')?'':driver_info_defines[2];
    var ssn = (driver_info_defines[10]=='' || driver_info_defines[10]==null || driver_info_defines[10]=='null')?'':driver_info_defines[10];
    var dob = (driver_info_defines[3]=='' || driver_info_defines[3]==null || driver_info_defines[3]=='null')?'':driver_info_defines[3];
    var license_date = (driver_info_defines[6]=='' || driver_info_defines[6]==null || driver_info_defines[6]=='null')?'':driver_info_defines[6];
    var license_nb = (driver_info_defines[8]=='' || driver_info_defines[8]==null || driver_info_defines[8]=='null')?'':driver_info_defines[8];
    var address = (driver_info_defines[9]=='' || driver_info_defines[9]==null || driver_info_defines[9]=='null')?'':driver_info_defines[9];
    var gender = (driver_info_defines[4]=='' || driver_info_defines[4]==null || driver_info_defines[4]=='null')?-1:driver_info_defines[4];
    var nationality = (driver_info_defines[11]=='' || driver_info_defines[11]==null || driver_info_defines[11]=='null')?-1:driver_info_defines[11];
    var status = (driver_info_defines[5]=='' || driver_info_defines[5]==null || driver_info_defines[5]=='null')?-1:driver_info_defines[5];
    var state = (driver_info_defines[7]=='' || driver_info_defines[7]==null || driver_info_defines[7]=='null')?-1:driver_info_defines[7];

    (first_name=='')?$("#profile_first_name").text('First Name').addClass('profile_empty'):$("#profile_first_name").text(first_name);
    (middle_name=='')?$("#profile_middle_name").text('Middle Name').addClass('profile_empty'):$("#profile_middle_name").text(middle_name);
    (last_name=='')?$("#profile_last_name").text('Last Name').addClass('profile_empty'):$("#profile_last_name").text(last_name);
    (ssn=='')?$("#profile_ssn").text('SSN').addClass('profile_empty'):$("#profile_ssn").text(ssn);
    (address=='')?$("#profile_address").text('Address').addClass('profile_empty'):$("#profile_address").text(address);
    (dob=='')?$("#profile_dob").text('Date of Birth').addClass('profile_empty'):$("#profile_dob").text(dob);
    (license_nb=='')?$("#profile_license_number").text('License Number').addClass('profile_empty'):$("#profile_license_number").text(license_nb);
    (license_date=='')?$("#profile_license_date").text('License Date').addClass('profile_empty'):$("#profile_license_date").text(license_date);
    

    
    var img_url = img_online_url+user_id+".jpg";
    
    if(nationality == -1)
    addDropDown('nationality','Select Nationality','Select Nationality',nationality_dropdown);
    else
    {
        addDropDown('nationality','Select Nationality','Select Nationality',nationality_dropdown,nationality_dropdown[nationality-1]['value']);
        $("#nationality").val(nationality);
    }
    if(gender== -1)
    addDropDown('gender','Select Gender','Select Gender',gender_dropdown);
    else
    {
        addDropDown('gender','Select Gender','Select Gender',gender_dropdown,gender_dropdown[gender-1]['value']);
        $("#gender").val(gender);
    }

    if(status == -1)
    addDropDown('martial','Select Status','Select Status',martial_dropdown);
    else
    {
        addDropDown('martial','Select Status','Select Status',martial_dropdown,martial_dropdown[status-1]['value']);
        $("#martial").val(status);
    }

    if(state ==-1)
    addDropDown('license','Select State','Select State',license_dropdown);
    else
    {
        addDropDown('license','Select State','Select State',license_dropdown,license_dropdown[state-1]['value']);
        $("#license").val(state);
    } 
   //content_gender
    if(getStorage('login_type')=='2')
    {
        $("#google_alert").css('display','block');
        $("#username_profile").css('display','none');
        $("#password_profile").css('display','none');
        $("#profile_first_name").css('border-top','none');
    }

    if(getStorage('img_url')!=null &&  getStorage('img_url')!='' && getStorage('img_url')!='null')
    {
        if(getStorage('img_url').substring(0, 16)=='http://savemeapp')
            $("#change_img").attr('src',getStorage("img_url")+"?"+ new Date().getTime());
        else
            $("#change_img").attr('src',getStorage("img_url"));
        startLoading();
        $("#change_img").on('load',function(){stopLoading();})    
    }
    //alert(img_url);

    var username = getStorage("username");
    var profile_password = '';
    var email = getStorage("email");
    var mobile = getStorage("mobile");
    if(mobile=='' || mobile==null || mobile=='null')
    {
        $("#profile_mobile").text('Phone Number').addClass('profile_empty');
    }
    var hardware = getStorage('use_hardware');
    
    
    //alert(use_hardware);

    $("#profile_username").text(username);
    $("#profile_email").text(email);
    
    if(mobile!='' && mobile!=null && mobile!='null')
    $("#profile_mobile").text(mobile);

    if(hardware == 0)
    {
        $("#use_hardware").attr('checked',false);
    }
    else
    {
        $("#use_hardware").attr('checked',true);
    }

   //$("#use_hardware").attr('checked')

$.getScript("../js/sth-select.js", function(){});
    

// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("change_img");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


$("#camera").on('click',function()
{
    openCamera(function(imageData)
    {
        photo_canged = 1;
        var image = document.getElementById('change_img');
        image.src = "data:image/jpeg;base64," + imageData;
        modal.style.display = "none";
    },function(message)
    {
        alert(message);
    });
})

$("#gallery").on('click',function()
{
    openGallery(function(imageData)
    {
        photo_canged = 1;
        var image = document.getElementById('change_img');
        image.src = imageData;
        modal.style.display = "none";
    },function(message)
    {
        alert(message);
    })
})


    $("#change_username").on('click',function()
    {
        var current_username = $("#profile_username").text();

        prompt('Edit Username' , 'body prompt' ,'text',current_username, function(data)
        {
            if(data!='')
            {
                var param = 'user_id='+user_id+"&username="+data;
                request(check_username_url,param,function(result){
                    if(result['success'] == -1)
                    {
                        alert('Username Already exists');
                    }
                    else if(result['success'] == 1)
                    {
                        
                        change('username' , data);
                    }
                })
            }
            else
                alert('Cannot be empty');
           
        });
        //$("#profile_username").text($("#b").val());

    });

    $("#change_password").on('click',function()
    {
        var current_password = $("#profile_password").text();

        prompt('Old Password' , 'body prompt' ,'password','', function(data)
        {
            
            var link = check_pass_link;
            var params = "user_id="+user_id+"&old_password="+data
            request(link , params , function(values)
            {
                    if(values['success']==1)
                    {
                        change('password' , data);
                    }
                    else
                    {
                        alert('Invalid Password');
                    }
            });
             //change('password' , data);
           
        });
        //$("#profile_username").text($("#b").val());

    });



    $("#change_email").on('click',function()
    {
        
        var current_email = (email=='')?'':email;

        prompt('Edit Email' , 'body prompt' ,'text',current_email, function(data)
        {
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!filter.test(data)) 
            {
                alert('Invalid Email Entry');
            }
            else
            {
                email = data;
                (data=='')?$("#profile_email").addClass('profile_empty'):$("#profile_email").removeClass('profile_empty');
                data=(data=='')?'Email':data;
                change('email' , data);
            }
        });
        

    });

    $("#change_mobile").on('click',function()
    {
        var current_mobile = (mobile=='')?'':mobile;

        prompt('Edit Phone Number' , 'body prompt' ,'number',current_mobile, function(data)
        {
            mobile = data;
            (data=='')?$("#profile_mobile").addClass('profile_empty'):$("#profile_mobile").removeClass('profile_empty');
            data=(data=='')?'Phone Number':data;
            change('mobile' , data);
            
        });
    });

    $("#change_first_name").on('click',function()
    {
        var current_first_name =(first_name=='')?'':first_name;

        prompt('Edit First Name' , 'body prompt' ,'text',current_first_name, function(data)
        {
            first_name = data;
            (data=='')?$("#profile_first_name").addClass('profile_empty'):$("#profile_first_name").removeClass('profile_empty');
            data=(data=='')?'First Name':data;
            change('first_name' , data);
            
        });
    });

    $("#change_middle_name").on('click',function()
    {
        var current_middle_name =(middle_name=='')?'':middle_name;

        prompt('Edit Middle Name' , 'body prompt' ,'text',current_middle_name, function(data)
        {
            middle_name = data;
            (data=='')?$("#profile_middle_name").addClass('profile_empty'):$("#profile_middle_name").removeClass('profile_empty');
            data=(data=='')?'Middle Name':data;
            change('middle_name' , data);
            
        });
    });

    $("#change_last_name").on('click',function()
    {
        var current_last_name =(last_name=='')?'':last_name;

        prompt('Edit Last Name' , 'body prompt' ,'text',current_last_name, function(data)
        {
            last_name = data;
            (data=='')?$("#profile_last_name").addClass('profile_empty'):$("#profile_last_name").removeClass('profile_empty');
            data=(data=='')?'Last Name':data;
            change('last_name' , data);
        });
    });

    $("#change_address").on('click',function()
    {
        var current_address =(address=='')?'':address;

        prompt('Edit Last Name' , 'body prompt' ,'text',current_address, function(data)
        {
            address = data;
            (data=='')?$("#profile_address").addClass('profile_empty'):$("#profile_address").removeClass('profile_empty');
            data=(data=='')?'Address':data;
            change('address' , data);
        });
    });

    $("#change_ssn").on('click',function()
    {
        var current_ssn =(ssn=='')?'':ssn;

        prompt('Edit SSN' , 'body prompt' ,'number',current_ssn, function(data)
        {
            ssn = data;
            (data=='')?$("#profile_ssn").addClass('profile_empty'):$("#profile_ssn").removeClass('profile_empty');
            data=(data=='')?'SSN':data;
            change('ssn' , data);
        });
    });

    $("#change_license_number").on('click',function()
    {
        var current_license_number =(license_nb=='')?'':license_nb;

        prompt('Edit License Number' , 'body prompt' ,'number',current_license_number, function(data)
        {
            license_nb = data;
            (data=='')?$("#profile_license_number").addClass('profile_empty'):$("#profile_license_number").removeClass('profile_empty');
            data=(data=='')?'License Number':data;
            change('license_number' , data);
        });
    });

    $("#change_license_date").on('click',function()
    {
        var current_license_date =(license_date=='')?'':license_date;

        prompt('Edit License Number' , 'body prompt' ,'date',current_license_date, function(data)
        {
            license_date = data;
            (data=='')?$("#profile_license_date").addClass('profile_empty'):$("#profile_license_date").removeClass('profile_empty');
            data=(data=='')?'License Date':data;
            change('license_date' , data);
        });
    });

    $("#change_dob").on('click',function()
    {
        var current_dob =(dob=='')?'':dob;

        prompt('Edit License Number' , 'body prompt' ,'date',current_dob, function(data)
        {
            dob = data;
            (data=='')?$("#profile_dob").addClass('profile_empty'):$("#profile_dob").removeClass('profile_empty');
            data=(data=='')?'Date of Birth':data;
            change('dob' , data);
        });
    });

    function change(key , value)
    {
        var old_password = '';
        if(key == 'password')
        {
            // check if old pass is correct 
            prompt('Change Password' , 'body prompt' ,'password','', function(data)
            {
                var lowerCaseLetters = /[a-z]/g;            
                var upperCaseLetters = /[A-Z]/g;
                var numbers = /[0-9]/g;
                var validate_lowercase = (data.match(lowerCaseLetters))?true:false;
                var validate_uppercase = data.match(upperCaseLetters)?true:false;
                var validate_numbers = data.match(numbers)?true:false;

                if(validate_lowercase==false || validate_uppercase==false || validate_numbers==false || data.length<8)
                {
                    alert('Password must be at least 8 characters with Lower,Upper characters and Numbers');
                   
                }
                else
                {
                    old_password = data;
                    prompt('Confirm Password' , 'body prompt' ,'password','', function(data)
                    {
                        if(old_password != data)
                        {
                            alert('Password not Matched');
                        }
                        else
                        {
                            if(data !='')
                                new_password = data;
                            else
                                alert('Cannot be empty');
                        }  
                    }); 
                }   
            });
        }
        else
        $("#profile_"+key).text(value);
    }
    
$("#save_changes").on('click' , function()
{
    //alert(first_name+" "+middle_name+" "+last_name+" "+email+" "+mobile+" "+address+" "+ssn+" "+dob+" "+license_nb);

    
    var profile_img = $("#change_img").attr('src');
    var profile_username = $("#profile_username").text();
    var profile_email = $("#profile_email").text();
    var profile_mobile = $("#profile_mobile").text();
    var password_changed = (new_password=='')?0:1;
    var use_hardware = ($("#use_hardware").is(':checked')==false)?0:1;
    var edit_nationality = ($("#nationality").val()==-1)?'':$("#nationality").val();
    var edit_gender = ($("#gender").val()==-1)?'':$("#gender").val();
    var edit_status = ($("#martial").val()==-1)?'':$("#martial").val();
    var edit_state = ($("#license").val()==-1)?'':$("#license").val();

    var link = edit_profile_link;
    var params ="username="+profile_username+"&password="+new_password+"&email="+profile_email
    +"&mobile="+profile_mobile+"&img_change="+photo_canged+"&userid="+user_id+"&driverid="+driver_id+"&password_changed="+password_changed
    +"&use_hardware="+use_hardware+"&first_name="+first_name+"&last_name="+last_name+"&middle_name="
    +middle_name+"&address="+address+"&dob="+dob+"&ssn="+ssn+"&license_nb="+license_nb+"&license_date="
    +license_date+"&nationality="+edit_nationality+"&gender="+edit_gender+"&state="+edit_state+"&status="+edit_status
    +"&login_type="+getStorage('login_type');
 
    
    if(photo_canged == 1)
    {   
        var password_changed = (new_password=='')?0:1;
        startLoading();

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = profile_img.substr(profile_img.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";
        var params = new Object();
        params.username = profile_username;
        params.email = profile_email;
        params.mobile = profile_mobile;
        params.password = new_password;
        params.password_changed=password_changed;
        params.userid = user_id;
        params.driverid = driver_id;
        params.img_change = photo_canged;
        params.use_hardware = use_hardware;
        params.first_name = first_name;
        params.middle_name = middle_name;
        params.last_name = last_name;
        params.address=address;
        params.dob=dob;
        params.ssn=ssn;
        params.license_nb=license_nb;
        params.license_date=license_date;
        params.nationality=edit_nationality;
        params.gender=edit_gender;
        params.state=edit_state;
        params.status=edit_status;
        params.login_type=1;
        options.params = params;
        options.chunkedMode = false;
        var ft = new FileTransfer();
        ft.upload(profile_img, link, function(result)
        { 
            stopLoading();
            setStorage("username",profile_username);
            setStorage("password",profile_password);
            setStorage("email",profile_email);
            setStorage("mobile",profile_mobile);
            setStorage('login_driver_dob',dob);
            $("#menu_username").text(getStorage("username"));
            setStorage('img_url','http://savemeapp.co/appServices/uploads/'+getStorage("user_id")+".jpg");
            
            $("#change_img").attr('src',getStorage("img_url")+"?"+ new Date().getTime());
            $("#menu_img").attr('src',getStorage("img_url")+"?"+ new Date().getTime());
            setStorage('use_hardware',use_hardware);
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            $("#menu_home").addClass('active');
            $("#homeSubmenu").removeClass('in');
            $("#navigate").attr('href','#/app/home');
            alertCallback('Profile Edit Successfully','',function()
            {
                driver_info_defines=[first_name,middle_name,last_name,dob,edit_gender,edit_status,license_date,edit_state,license_nb,address,ssn,edit_nationality];
                   
                 $("#navigate").click();
            });
            
        }, function(error)
        {
            alert('An Error Ocurred');
        }, options);

    }
    
    else
    {
        var link = edit_profile_link;
        

        request(link,params,function(data)
        {
            if(data['success']==1)
            {
                setStorage("username",profile_username);
               // setStorage("password",profile_password);
                setStorage("email",profile_email);
                setStorage("mobile",profile_mobile);
                setStorage("use_hardware",use_hardware);
                setStorage('login_driver_dob',dob);
                $("#menu_username").text(getStorage("username"));

                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace(" active", "");
                $("#menu_home").addClass('active');
                $("#homeSubmenu").removeClass('in');
                $("#navigate").attr('href','#/app/home');

                alertCallback('Profile Edit Successfully','',function()
                {
                    driver_info_defines=[first_name,middle_name,last_name,dob,edit_gender,edit_status,license_date,edit_state,license_nb,address,ssn,edit_nationality];
                         $("#navigate").click();
                });
            }
            else
            alert('An Error Ocurred');
        },'');
    }
    
    
});

})
