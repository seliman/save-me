angular.module('sidemenu.controllers')
.controller('accidentsCtrl', function($scope) 
{
    $('li > a > div').removeClass('submenu_active');
    $("table").rtResponsiveTables();
    
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
    var params = "driver_id="+getStorage("driver_id");

   

    request(previous_accidents_url,params,function(data)
    {
        if(data['success'] == 1)
        {

            
            $("#search_content").css('display','block');
            $("#table_div").css('display','block');
            $("#no_accident").css('display','none');

            var table_content = '<table>';

            table_content += '<thead>';
            table_content += '<tr>';
            table_content += '<th>Accident Date</th>';
            table_content += '<th>Day_of_Week</th>';
            table_content += '<th>Accident Time</th>';
            table_content += '<th>City</th>';
            table_content += '<th>Country</th>';
            table_content += '<th>Street</th>';
            table_content += '<th>Light Condition</th>';
            table_content += '<th>Weather Condition</th>';
            table_content += '<th>Speed Limit</th>';
            table_content += '</tr>';
            table_content += '</thead>';
            table_content += '<tbody>';
            table_content += '</tbody>';
            table_content += '</table>';
        
           // $("table").append(table_content);
        
            var counter = 0;
       
            $.each(data['accidents'], function(index) 
            {
            
            
                counter++;
                var table_content ='';
                table_content += '<tr>';
                table_content += '<td>'+data['accidents'][index].Accident_date+'</td>';
                table_content += '<td>'+data['accidents'][index].day+'</td>';
                table_content += '<td>'+data['accidents'][index].Accident_time+'</td>';
                table_content += '<td>'+data['accidents'][index].city+'</td>';
                table_content += '<td>'+data['accidents'][index].country+'</td>';
                table_content += '<td>'+data['accidents'][index].street+'</td>';
                table_content += '<td>'+data['accidents'][index].light+'</td>';
                table_content += '<td>'+data['accidents'][index].weather+'</td>';
                table_content += '<td>'+data['accidents'][index].Speed_limit+'</td>';
                table_content += '</tr>';
                $("tbody").append(table_content);
            

            },'');
        
            $("#total_accidents").text(counter);


            $("#search").on('keyup',function()
            {
                var total = 0;
                $("tbody").html('');
                var search_text = $("#search").val();
                var search_length = $("#search").val().length;
        
                if(search_text == '')
                {
                    $.each(data['accidents'], function(index)
                    {
                            var table_content ='';
                            table_content += '<tr>';
                            table_content += '<td>'+data['accidents'][index].Accident_date+'</td>';
                            table_content += '<td>'+data['accidents'][index].day+'</td>';
                            table_content += '<td>'+data['accidents'][index].Accident_time+'</td>';
                            table_content += '<td>'+data['accidents'][index].city+'</td>';
                            table_content += '<td>'+data['accidents'][index].country+'</td>';
                            table_content += '<td>'+data['accidents'][index].street+'</td>';
                            table_content += '<td>'+data['accidents'][index].light+'</td>';
                            table_content += '<td>'+data['accidents'][index].weather+'</td>';
                            table_content += '<td>'+data['accidents'][index].Speed_limit+'</td>';
                            table_content += '</tr>';
                            $("tbody").append(table_content);
                            total++;
        
                    });
                    
                    $("#total_accidents").text(total);
                }
        
                else
                {
                    $.each(data['accidents'], function(index)
                    {
                    
                        if( (data['accidents'][index].city.slice(0 , search_length)).toLowerCase() == search_text.toLowerCase())
                        {
                            var table_content ='';
                            table_content += '<tr>';
                            table_content += '<td>'+data['accidents'][index].Accident_date+'</td>';
                            table_content += '<td>'+data['accidents'][index].day+'</td>';
                            table_content += '<td>'+data['accidents'][index].Accident_time+'</td>';
                            table_content += '<td>'+data['accidents'][index].city+'</td>';
                            table_content += '<td>'+data['accidents'][index].country+'</td>';
                            table_content += '<td>'+data['accidents'][index].street+'</td>';
                            table_content += '<td>'+data['accidents'][index].light+'</td>';
                            table_content += '<td>'+data['accidents'][index].weather+'</td>';
                            table_content += '<td>'+data['accidents'][index].Speed_limit+'</td>';
                            table_content += '</tr>';
                            $("tbody").append(table_content);
                            total++;;
                        }
        
                    });
        
                    $("#total_accidents").text(total);
                }
            }); 
            
    

          //  $("table").rtResponsiveTables();
        }
        else if(data['success'] == -1)
        {
            $("#search_content").css('display','none');
            $("#table_div").css('display','none');
            $("#no_accident").css('display','block');
        }
    });


    


})


