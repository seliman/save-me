angular.module('ambulance_sidemenu.controllers')
.controller('ambulance_listCtrl', function($scope) 
{
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });

    var accident_id = vars['id'];
    
    var params = "accident_id="+accident_id;

    
    request(get_victims_url,params,function(data)
    {
        if(data['success'] == 1)
        {
            $.each(data['victims'], function(index) 
            {
                var name = '';
                var content = '<div class="item item-button-right">';
                if(data['victims'][index].Driver_idDriver=='' || data['victims'][index].Driver_idDriver==null || data['victims'][index].Driver_idDriver=='null')
                {   
                    if(data['victims'][index].victim_name=='' || data['victims'][index].victim_name=='null' || data['victims'][index].victim_name==null)
                    {
                        content += 'Not Specified';
                        name = '';
                    }
                    else
                    {
                        content += data['victims'][index].victim_name;
                        name = data['victims'][index].victim_name;
                    }
                }
                else
                {
                    content += data['victims'][index].First_name+" "+data['victims'][index].Middle_name+" "+data['victims'][index].Last_name;
                    name =  data['victims'][index].First_name+" "+data['victims'][index].Middle_name+" "+data['victims'][index].Last_name;
                }
               
                content +='<button class="button" style="background:color:#FFFFFF;color:#e78200;" onclick="editVictim('+data['victims'][index].idVictim+','+"'"+name+"'"+','+"'"+data['victims'][index].VictimCategory+"'"+','+"'"+data['victims'][index].InjurySeverity+"'"+','+"'"+data['victims'][index].hospital_idhospital+"'"+','+"'"+data['victims'][index].additionalinfo+"'"+','+"'"+data['victims'][index].category_name+"'"+','+"'"+data['victims'][index].injury_name+"'"+','+"'"+data['victims'][index].hospital_name+"'"+','+"'"+data['victims'][index].patient_status+"'"+','+"'"+data['victims'][index].status_name+"'"+','+"'"+data['victims'][index].chief_complaint+"'"+','+"'"+data['victims'][index].victim_age+"'"+','+"'"+data['victims'][index].victim_cause+"'"+')">';
                content += '<i class="icon ion-edit"></i>';
                content +='</button></div>';
    
                $("#victim_list").append(content);
            });
            
        }
        else
        {
            alert('An Error Occurred');
        }
    });


    $("#add_victim").on('click',function()
    {
        addVictim(accident_id);
    });

});
 
function editVictim(idvictim,victim_name,category,injury,hospital,additional_info,category_name,injury_name,hospital_name,status_id,status_name,complaint,age,cause)
{
   // alert(idvictim+" "+victim_name+" "+category+" "+inhury+" "+hospital+" "+additional_info);
    
    var parameter = "&name="+encodeURIComponent(victim_name)+'&category='+encodeURIComponent(category)+"&injury="+encodeURIComponent(injury)+"&hospital="+encodeURIComponent(hospital)
    +"&info="+encodeURIComponent(additional_info)+"&category_name="+encodeURIComponent(category_name)
    +"&injury_name="+encodeURIComponent(injury_name)+"&hospital_name="+encodeURIComponent(hospital_name)
    +"&status="+encodeURIComponent(status_id)+"&status_name="+encodeURIComponent(status_name)+"&complaint="+encodeURIComponent(complaint)+"&age="+age+"&cause="+cause;
    
    $("#edit_accident").attr('href','#/app/edit?id='+idvictim+"&action=1"+parameter);
    $("#edit_accident").click(); 
}


function addVictim(accident_id)
{
	$("#edit_accident").attr('href','#/app/edit?id='+accident_id+"&action=2");
    $("#edit_accident").click(); 
}